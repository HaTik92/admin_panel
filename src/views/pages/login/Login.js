import React from 'react'
import { Link } from 'react-router-dom'
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow
} from '@coreui/react'
import CIcon from '@coreui/icons-react'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Field } from "react-final-form";
const Login = () => {
  const dispatch = useDispatch()
  const onSubmit = async (values) => {
    console.log(values);
    return dispatch({ type: 'CHECK_LOGIN', values })
  };
  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <Form
                    onSubmit={onSubmit}
                    render={({ handleSubmit }) => (
                      <>
                        <CForm onSubmit={handleSubmit}>
                          <h1>Login</h1>
                          <p className="text-muted">Sign In to your account</p>
                          <CInputGroup className="mb-3">
                            <CInputGroupPrepend>
                              <CInputGroupText>
                                <CIcon name="cil-user" />
                              </CInputGroupText>
                            </CInputGroupPrepend>
                            <Field
                              name="username"
                              component="input"
                              type="text"
                              placeholder="Username"
                              className="form-control"
                            />
                          </CInputGroup>
                          <CInputGroup className="mb-4">
                            <CInputGroupPrepend>
                              <CInputGroupText>
                                <CIcon name="cil-lock-locked" />
                              </CInputGroupText>
                            </CInputGroupPrepend>
                            <Field
                              name="password"
                              component="input"
                              type="password"
                              placeholder="Password"
                              className="form-control"
                            />
                          </CInputGroup>
                          <CRow>
                            <CCol xs="12">
                              <CButton color="primary" className="px-4" type="submit">Login</CButton>
                            </CCol>
                          </CRow>
                        </CForm>
                      </>
                    )} />
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  )
}

export default Login
