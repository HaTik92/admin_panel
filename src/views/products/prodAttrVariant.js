import React from 'react';
import {
  CFormGroup,
  CSelect
} from '@coreui/react'
import { useDispatch } from 'react-redux';

const ProdAttrVariant = (props) => {
  const dispatch = useDispatch();
  const addOneSize = (val, id) => {
    if (val !== '') {
      return dispatch({ type: 'ADD_SIZE_TO_VARIANT', val, id })
    }
  }
  return <>
    <CFormGroup>
      <CSelect custom name="attribute_taxonomy" onChange={(e) => addOneSize(+e.target.value, props.colorId)} >
        <option value="">Custom product attribute</option>
        {props.sizes.map((el, i) => <option value={el.id} key={i} >{el.name}</option>)}
      </CSelect>
    </CFormGroup>
  </>
}
export default ProdAttrVariant