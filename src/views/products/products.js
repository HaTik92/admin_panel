import React, { useEffect, useState } from 'react'
// import {GET_PRODUCTS} from '../../../server/queries'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CImg,
} from '@coreui/react'
import { useDispatch, useSelector } from 'react-redux';
import AddOrEditProductModal from './addOrEditProductModal';
import EditProductModal from './editProductModal';
import axios from 'axios';

const Products = () => {
  // const newProd = useSelector(state => state.newProduct)
  // const [editCheck , setEditProdCheck] = useState(false);
  // const [activeKey, setActiveKey] = useState(1)
  // const [activeSchedule, setActiveSchedule] = useState(0)
  // const [activeStockManage, setActiveStockManage] = useState(1)
  // const [visible, setVisible] = useState(false)
  // const prodAttributes = [
    //     newProd.prodAttributes.location,
  //     newProd.prodAttributes.material,
  //     // newProd.prodAttributes.variant,
  //     // newProd.prodAttributes.color,
  //     // newProd.prodAttributes.size,
  // ]
  // const [products, setProductsData] = useSelector(state => state.products)
  // const products = useSelector(state => state.product.products)
  const dispatch = useDispatch()
  const [details_edit, setDetailsEdit] = useState([])
  const [details_delete, setDetailsDelete] = useState([])
  const [addProd, setAddProd] = useState(false)
  const [editProduct, setEditProduct] = useState(false)
  const [products, setProducts] = useState([]);
  const [editingProductId, setEditingProductId] = useState(null);
  useEffect(() => {
      axios.get('http://localhost:8080/product', {headers: {"Access-Control-Allow-Origin": "*"}}).then(res => {
          if(res.status === 200) {
              setProducts(res.data.products)
            }
    })
    .catch(err => {
        console.log('err', err)
      });
    
    }, [])

    const delete_product = (id) => {
      axios.post('http://localhost:8080/deleteProduct', {id: id})
      .then(res => {
        modalDetailsDelete('');
        setProducts(res.data.products);
      })
      .catch(err => {
        console.log('err', err)
      });
    }

  const fields = [
    'id',
    'photo',
    'in_stock',
    'name',
    'price',
    'ratings',
    'review',
    'code',
    'slug',
    'new',
    {
      key: 'show_details',
      label: '',
      sorter: false,
      filter: false
    }
  ];
  const openModalAddProd = () => {
    setAddProd(!addProd)
  }
  const modalDetailsOpenEdit = (id) => {
    setAddProd(!addProd);
    setEditingProductId(id);
    // setEditProduct(!editProduct)
    // setDetailsEdit(index)
  }
  const modalDetailsEdit = (id, index = '') => {
    setDetailsEdit(index)
  }
  const modalDetailsDelete = (index = '') => {
    setDetailsDelete(index)
  }

  return <>
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            Բոլոր ապրանքները
            <CButton className="float-right" size="sm" color="dark" onClick={() => openModalAddProd()}>Add Product</CButton>
            <AddOrEditProductModal addProd={addProd} setAddProd={setAddProd} products={products} setProducts={setProducts} editingProductId={editingProductId} />
          </CCardHeader>
          <CCardBody>
            <CDataTable
              items={products}
              fields={fields}
              // columnFilter
              // tableFilter
              sorter
              hover
              striped
              bordered
              size="md"
              itemsPerPage={10}
              pagination
              scopedSlots={
                {
                  // 'stock':
                  //   (item) => (
                  //     <td>
                  //       <CBadge color={getBadge(item.status.value)}>
                  //         {item.stock}
                  //       </CBadge>
                  //     </td>
                  //   ),
                  'photo':
                    (item) => (
                      <td>
                        <CImg
                          src={item.pictures.length > 0 ? item.pictures[0].url : 'product_images/default.png'}
                          width="50"
                          alt=""
                        />
                        {/* <img src={item.pictures.length > 0 ? item.pictures[0].url : ''} width="50" alt="" /> */}
                      </td>
                    ),
                  'show_details':
                    (item, index) => {
                      return (
                        <td>
                          {/* <CButton
                            color="info"
                            size="sm"
                            className='mr-1'
                            onClick={() => modalDetailsOpenEdit(item.id)}
                          >
                            Edit
                          </CButton> */}
                          {/* <EditProductModal
                          editProduct={editProduct}
                          setEditProduct={setEditProduct}
                          item={item}
                          details_edit={details_edit}
                          index={index}
                          modalDetailsEdit={modalDetailsEdit}
                        /> */}
                          <CButton color="danger"
                            onClick={() => { modalDetailsDelete(index) }}
                            className={`mr-1, ${item.id}`}>Ջնջել</CButton>
                        </td>
                      )
                    },
                  'details':
                    (item, index) => {
                      return (<>
                        
                        <CModal
                          show={details_delete === index}
                          onClose={() => modalDetailsDelete('')}
                          color="warning"
                        >
                          <CModalHeader closeButton>
                            <CModalTitle>Դուք համոզվա՞ծ եք</CModalTitle>
                          </CModalHeader>
                          <CModalFooter>
                            <CButton color="secondary" onClick={() => modalDetailsDelete('')}>Չեղարկել</CButton>{' '}
                            <CButton color="danger" onClick={() => delete_product(item.id)}>Այո</CButton>
                          </CModalFooter>
                        </CModal>
                      </>
                      )
                    }
                }
              } />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>
}

export default Products