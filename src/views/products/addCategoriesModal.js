import React, { useEffect, useState } from 'react';
import './Products.scss';

import {
  CCard,
  CCardBody,
  CCol,
  CButton,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CFormGroup,
  CLabel,
  CInput,
} from '@coreui/react';



const AddCategoriesModal = (props) => {
  const {addProd, setAddProd, addCategory, productName, setProductName} = props;
  const [validated, setValidated] = useState(true)

  const hendleClickAddCategory = () => {
      if(productName != '') {
        addCategory() 
        setValidated(true)
      } else {
        setValidated(false)
      }
  }

  return (<>
    <CModal
      size="xl"
      show={addProd}
      onClose={() => setAddProd(!addProd)}
      color="success"
    >
      <CModalHeader closeButton>
        <CModalTitle>Ավելացնել նոր կատեգորիա</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardBody>
              <CFormGroup>
                <CLabel htmlFor="producta_name">Կատեգորիայի անունը</CLabel>
                <CInput id="producta_name" placeholder="Մուտքագրեք կատեգորիայի անունը" onChange={(e) => setProductName(e.target.value)} value={productName} />
                {!validated && !productName  ? <span style={{color: 'red'}}>Մուտքագրեք կատեգորիայի անունը</span> : <></>}
              </CFormGroup>
            </CCardBody>
          </CCard>
        </CCol>
      </CModalBody>
      <CModalFooter>
        <CButton color="danger" onClick={() => setAddProd(!addProd)}>Չեղարկել</CButton>{' '}
        <CButton color="success" onClick={() =>hendleClickAddCategory() }>Ավելացնել</CButton>
      </CModalFooter>
    </CModal>
  </>)
}

export default AddCategoriesModal