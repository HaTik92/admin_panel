import React from 'react';
import {
  CCol,
  CButton,
  CFormGroup,
  CLabel,
  CInput,
  CInputRadio
} from '@coreui/react'
import { useDispatch, useSelector } from 'react-redux';



const ProdAttrLocation = (props) => {
  const location = useSelector(state => state.product.newOrEditProduct.prodAttributes.location)
  const dispatch = useDispatch()

  // const newProdLocationAttributeCategory = (id) => {
  //   props.newProdLocationAttributeCategory(id)
  // }
  const newProdLocationAttributeCategory = (categoryId) => {
    return dispatch({ type: 'NEW_PRODUCT_LOCATION_ATTRIBUTE_CATEGORY', id: categoryId })
  }
  const addNewCat = (name) => {
    props.addNewCat(name)
  }
  const removeBlock = (name) => {
    props.removeBlock(name)
  }
  const newCategoryValue = (el, name) => {
    props.newCategoryValue(el, name)
  }
  const addCategpry = (name) => {
    props.addCategpry(name)
  }
  console.log(location);
  return <>
    <CFormGroup row className="align-items-center" key="">
      <CCol md="2">
        <CLabel>{location.title}</CLabel>
      </CCol>
      <CCol md="6">
        {location.categories_radio.map((el, i) => {
          return (
            <CFormGroup variant="custom-radio" inline key={i}>
              <CInputRadio
                custom
                id={location.name + '_' + props.val + '_' + i}
                name={location.name}
                checked={el.checked}
                onChange={() => newProdLocationAttributeCategory(el.id)}
              />
              <CLabel variant="custom-checkbox" htmlFor={location.name + '_' + props.val + '_' + i}>{el.name}</CLabel>
            </CFormGroup>
          )
        })
        }
      </CCol>
      <CCol md="4">
        <CButton shape="pill" color="info" className="w-50" onClick={() => addNewCat(location.name)}>
          Ավելացնել նորը
        </CButton>
        <CButton shape="pill" color="danger" className="w-50" onClick={() => removeBlock(location.name)}>
          Հեռացնել
        </CButton>
      </CCol>
    </CFormGroup>
    {location.addCategory
      && <CFormGroup row>
        <CCol md="10">
          <CInput defaultValue={location.addNew} onChange={(e) => newCategoryValue(e, location.name)} />
        </CCol>
        <CCol md="2">
          <CButton block variant="outline" color="info" onClick={() => addCategpry(location.name)}>+</CButton>
        </CCol>
      </CFormGroup>}
  </>
}
export default ProdAttrLocation