const productsData = [
    {
        id: 0,
        category : [{name: "Women", slug: "women", __typename: "Category"}],
        name: "կանացի սպորտային կոշիկ՝ կաշվից" ,
        price: 76,
        ratings: 4,
        review: 2,
        sm_pictures : [],
        slug: "beige-metal-hoop-tote-bag",
        __typename: "Product",
        new : null
     },
     {
        id: 1,
        category : [{name: "Women", slug: "women", __typename: "Category"}],
        name: "կանացի սպորտային կոշիկ՝ կաշվից" ,
        price: 71,
        ratings: 5,
        review: 1,
        sm_pictures : [],
        slug: "new-iamghe",
        __typename: "Product",
        new : null
     }
  ]
  
  export default productsData
  