import CIcon from '@coreui/icons-react';
import { CButton, CCol, CFormGroup, CInputCheckbox, CInputRadio, CLabel } from '@coreui/react';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';


const ProdAttr = (props) => {
    const addAttributes = useSelector(state => state.product.addAttr)
    const dispatch = useDispatch()
    console.log(props);
    const removeBlock = (type) => {
        props.removeProdAttr(ProdAttr)
        console.log(props.action);
        // return dispatch({ type: props.action, value: false })
    }
    return (
        <>
            <CFormGroup row className="align-items-center">
                <CCol md="2">
                    <CLabel>{props.prod_attr.title}</CLabel>
                </CCol>
                <CCol md="6">
                    {
                        props.prod_attr.value_radio
                            ? props.prod_attr.value_radio.map((el, i) => {
                                return (
                                    <CFormGroup variant="custom-radio" inline>
                                        <CInputRadio custom id={props.prod_attr.name + i} name={props.prod_attr.name} value={el.value} />
                                        <CLabel variant="custom-checkbox" htmlFor={props.prod_attr.name + i}>{el.name}</CLabel>
                                    </CFormGroup>
                                )
                            })
                            : props.prod_attr.value.map((el, i) => {
                                return (
                                    <CFormGroup variant="custom-checkbox" inline>
                                        <CInputCheckbox custom id={el.value} name={el.value} value={el.value} />
                                        <CLabel variant="custom-checkbox" htmlFor={el.value}>{el.name}</CLabel>
                                    </CFormGroup>
                                )
                            })
                    }
                </CCol>
                <CCol md="4">
                    <CButton shape="pill" color="info" className="w-50" onClick={() => addNewCat(props.prod_attr.name)}>
                        Ավելացնել նորը
                    </CButton>
                    <CButton shape="pill" color="danger" className="w-50" onClick={() => removeBlock(props.prod_attr.name)}>
                        Հեռացնել
                    </CButton>
                </CCol>
            </CFormGroup>
            props.prod_attr.addCategory
            ? <CFormGroup row>
                <CCol md="10">
                    <CInput id="newAttr" />
                </CCol>
                <CCol md="2">
                    <CButton block variant="outline" color="info">+</CButton>
                </CCol>
            </CFormGroup>
            ։ ""
        </>
    )
}

export default ProdAttr