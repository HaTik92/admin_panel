import React, { useState, useEffect } from 'react';
import AddMaterialsModal from './addMaterialsModal';
import axios from 'axios'

import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CButton,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CImg,
} from '@coreui/react';

const fields = [
  'id',
  'name',
  {
    key: 'show_details',
    label: '',
    sorter: false,
    filter: false
  }
];


const ProductMaterials = () => {
  const [materials, setMaterials] = useState([]);
  const [details_delete, setDetailsDelete] = useState([]);
  const [materialName, setMaterialName] = useState('');
  const [showAddMaterialPopup, setShowAddMaterialPopup] = useState(false);


  const modalDetailsDelete = (index = '') => {
    setDetailsDelete(index)
  }

  useEffect(() => {
    axios.get('http://localhost:8080/materials', {headers: {"Access-Control-Allow-Origin": "*"}}).then(res => {
        if(res.status === 200) {
          setMaterials(res.data.materials)
          }
    })
    .catch(err => {
      console.log('errrrrrr', err)
    });
  }, [])

  const addMaterial = () => {
    setShowAddMaterialPopup(!showAddMaterialPopup) 
    axios.post('http://localhost:8080/addMaterial', {name: materialName, slug: materialName.replaceAll(" ", "-")})
    .then(res => {
      setMaterials(res.data.materials);
      setMaterialName('');
    })
    .catch(err => {
      console.log('errrrrrr', err)
    });
  }

  const deleteMaterial = (id) => {
    axios.post('http://localhost:8080/deleteMaterial', {id: id})
    .then(res => {
      modalDetailsDelete('');
      setMaterials(res.data.materials);
    })
    .catch(err => {
      console.log('errrrrrr', err)
    });
  }


  return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            Բոլոր տեսակները
            <CButton className="float-right" size="sm" color="dark" onClick={() => setShowAddMaterialPopup(!showAddMaterialPopup)} >Ավելացնել Մատերիալ</CButton>
            <AddMaterialsModal 
              showAddMaterialPopup={showAddMaterialPopup} 
              setShowAddMaterialPopup={setShowAddMaterialPopup} 
              addCategory={addMaterial}
              materialName={materialName}
              setMaterialName={setMaterialName}
            />
          </CCardHeader>
          <CCardBody>
            <CDataTable
                items={materials}
                fields={fields}
                sorter
                hover
                striped
                bordered
                size="md"
                itemsPerPage={10}
                pagination
                scopedSlots={
                    {
                      // 'stock':
                      //   (item) => (
                      //     <td>
                      //         {item.name}
                      //     </td>
                      //   ),
                      'show_details':
                        (item, index) => {
                          return (
                            <td>
                              <CButton color="danger"
                                onClick={() => { modalDetailsDelete(index) }}
                                className={`mr-1, ${item.id}`}>Ջնջել</CButton>
                            </td>
                          )
                        },
                      'details':
                        (item, index) => {
                          return (<>
                            {/* <EditProductModal
                              item={item}
                            //   details_edit={details_edit}
                            //   index={index}
                            //   modalDetailsEdit={modalDetailsEdit}
                            /> */}
                            <CModal
                              show={details_delete === index}
                              onClose={() => modalDetailsDelete('')}
                              color="warning"
                            >
                              <CModalHeader closeButton>
                                <CModalTitle>Դուք համոզվա՞ծ եք</CModalTitle>
                              </CModalHeader>
                              <CModalFooter>
                                <CButton color="secondary" onClick={() => modalDetailsDelete('')}>Չեղարկել</CButton>{' '}
                                <CButton color="danger" onClick={() => deleteMaterial(item.id)}>Այո</CButton>
                              </CModalFooter>
                            </CModal>
                          </>
                          )
                        }
                    }
                  }
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default ProductMaterials
