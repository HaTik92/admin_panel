import React from 'react';
import {
  CCol,
  CButton,
  CFormGroup,
  CLabel,
  CInput,
  CInputCheckbox
} from '@coreui/react'
import { useDispatch, useSelector } from 'react-redux';



const ProdAttrMaterial = (props) => {
  // const newProdAttributeCategory = (name, id) => {
  //   props.newProdAttributeCategory(name, id)
  // }
  const material = useSelector(state => state.product.newOrEditProduct.prodAttributes.material)
  const dispatch = useDispatch()
  const newProdMaterialAttributeCategory = (categoryId) => {
    return dispatch({ type: 'NEW_PRODUCT_MATERIAL_ATTRIBUTE_CATEGORY', id: categoryId })
  }
  const addNewCat = (name) => {
    props.addNewCat(name)
  }
  const removeBlock = (name) => {
    props.removeBlock(name)
  }
  const newCategoryValue = (el, name) => {
    props.newCategoryValue(el, name)
  }
  const addCategpry = (name) => {
    props.addCategpry(name)
  }
  return <>
    <CFormGroup row className="align-items-center" key="">
      <CCol md="2">
        <CLabel>{material.title}</CLabel>
      </CCol>
      <CCol md="6">
        {material.categories.map((el, i) => {
          return (
            <CFormGroup variant="custom-checkbox" inline key={i}>
              <CInputCheckbox
                custom id={'catrgory_' + material.name + '_' + el.id}
                name={el.name}
                checked={el.checked}
                onChange={() => newProdMaterialAttributeCategory(el.id)}
              />
              <CLabel variant="custom-checkbox" htmlFor={'catrgory_' + material.name + '_' + el.id}>{el.name}</CLabel>
            </CFormGroup>
          )
        })
        }
      </CCol>
      <CCol md="4">
        <CButton shape="pill" color="info" className="w-50" onClick={() => addNewCat(material.name)}>
          Ավելացնել նորը
        </CButton>
        <CButton shape="pill" color="danger" className="w-50" onClick={() => removeBlock(material.name)}>
          Հեռացնել
        </CButton>
      </CCol>
    </CFormGroup>
    {material.addCategory
      && <CFormGroup row>
        <CCol md="10">
          <CInput defaultValue={material.addNew} onChange={(e) => newCategoryValue(e, material.name)} />
        </CCol>
        <CCol md="2">
          <CButton block variant="outline" color="info" onClick={() => addCategpry(material.name)}>+</CButton>
        </CCol>
      </CFormGroup>}
  </>
}
export default ProdAttrMaterial