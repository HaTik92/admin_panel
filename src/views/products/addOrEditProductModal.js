import React, { useEffect, useState } from 'react';
import ProdAttrLocation from './prodAttrLocation';
import ProdAttrMaterial from './prodAttrMaterial';
import ProdAttrVariant from './prodAttrVariant';
import { useDispatch, useSelector } from 'react-redux';
import './Products.scss';

import {
  CCard,
  CCardBody,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CFormGroup,
  CLabel,
  CInput,
  CTextarea,
  CSelect,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CInputCheckbox,
  CButtonGroup,
  CImg
} from '@coreui/react';
import axios from 'axios';



const AddOrEditProductModal = (props) => {
  const [validated, setValidated] = useState(true)
  const [productName, setProductName] = useState('');
  const [productDescription, setProductDescription] = useState('');
  const [productCategories, setProductCategories] = useState([]);
  const [productMaterials, setProductMaterials] = useState([]);
  const [productPhotos, setProductPhotos] = useState([]);
  const [price, setPrice] = useState("");
  const [salePrice, setSalePrice] = useState("");
  const [productCode, setProductCode] = useState("");
  const [productStock, setProductStock] = useState("true");
  const [stockQuantity, setStockQuantity] = useState("");
  const [manageStock, setManageStock] = useState(false);
  const [productNew, setProductNew] = useState(false);
  const [productColors, setProductColors] = useState([
    {id: 1,color:'#FFFF00',name: "Դեղին",en_name: "yellow",checked: false,},
    {id: 2,color:'#000000',name: "Սև",en_name: "black",checked: false,},
    {id: 3,color:'#FFFFFF',name: "Սպիտակ",en_name: "white",checked: false,},
    {id: 4,color:'#FF0000',name: "Կարմիր",en_name: "red",checked: false,},
    {id: 5,color:'#0000FF',name: "Կապույտ",en_name: "blue",checked: false,},
    {id: 6,color:'#00FF00',name: "Կանաչ",en_name: "green",checked: false,},
    {id: 7,color:'#FFC0CB',name: "Վարդագույն",en_name: "pink",checked: false,},
  ]);
  const [productSize, setProductSize] = useState([
    { id: 1, name: "34", slug: "34" },
    { id: 2, name: "35", slug: "35" },
    { id: 3, name: "36", slug: "36" },
    { id: 4, name: "37", slug: "37" },
    { id: 5, name: "38", slug: "38" },
    { id: 6, name: "39", slug: "39" },
    { id: 7, name: "40", slug: "40" },
    { id: 8, name: "41", slug: "41" },
    { id: 9, name: "42", slug: "42" },
    { id: 10, name: "43", slug: "43" },
    { id: 11, name: "44", slug: "44" },
    { id: 12, name: "45", slug: "45" },
    { id: 13, name: "46", slug: "46" },
    { id: 14, name: "S", slug: "S" },
    { id: 15, name: "XS", slug: "XS" },
    { id: 16, name: "M", slug: "M" },
    { id: 16, name: "L", slug: "L" },
    { id: 16, name: "XL", slug: "XL" },
    { id: 16, name: "XXL", slug: "XXL" },
  ])

  const newProd = useSelector(state => state.product.newOrEditProduct)
  const dispatch = useDispatch()
  // const [editCheck , setEditProdCheck] = useState(false);
  // const [warning, setWarning] = useState(false)
  // const [editProd, setEditProd] = useState(false)
  // const [addProd, setAddProd] = useState(false)
  const {addProd, setAddProd, products, setProducts, editingProductId, setEditingProductId} = props;
  const [activeKey, setActiveKey] = useState(1)
  const [activeSchedule, setActiveSchedule] = useState(0)
  const [activeStockManage, setActiveStockManage] = useState(1);

  useEffect(() => {
    if(editingProductId) {
      const editingProduct = products.filter(p => p.id === editingProductId)[0];
      setProductName(editingProduct.name);
      setProductDescription(editingProduct.short_desc);
      setPrice(editingProduct.price);
      setSalePrice(editingProduct.sale_price)
    }
  }, [editingProductId])
  // const [visible, setVisible] = useState(false)
  // const prodAttributes = [
  //     newProd.prodAttributes.location,
  //     newProd.prodAttributes.material,
  //     // newProd.prodAttributes.variant,
  //     // newProd.prodAttributes.color,
  //     // newProd.prodAttributes.size,
  // ]
  // const [products, setProductsData] = useSelector(state => state.products)
  // const products = useSelector(state => state.products)


  // const getBadge = status => {
  //   switch (status) {
  //     case 'instock': return 'success'
  //     case 'onbackorder': return 'warning'
  //     case 'outofstock': return 'danger'
  //     default: return 'primary'
  //   }
  // }
  // const fields = [
  //   'id',
  //   'photo',
  //   'stock',
  //   'name',
  //   'price',
  //   'ratings',
  //   'review',
  //   // 'sm_pictures',
  //   'slug',
  //   // '__typename',
  //   'new',
  //   'Action'
  // ];
  // const delete_product = (id, index) => {
  //   setWarning(!warning)
  //   return dispatch({ type: 'DELETE_PRODUCT', id })
  // }

  const priceDates = (key) => {
    key === 1 ? key = 0 : key = 1
    setActiveSchedule(key)
  }
  const stockManage = (key) => {
    key === 1 ? key = 2 : key = 1
    setActiveStockManage(key)
  }
  let selectDisableType = {
    location: newProd.prodAttributes.location.addAttr,
    material: newProd.prodAttributes.material.addAttr,
    variant: newProd.prodAttributes.variant.addAttr,
    // color: newProd.prodAttributes.color.addAttr,
    // size: newProd.prodAttributes.size.addAttr,
  }
  // const newProdInStock = (e) => {
  //   return dispatch({ type: 'NEW_PRODUCT_IN_STOCK', value: e.target.value })
  // }
  const selectValue = (e) => {
    return dispatch({ type: 'CHANGE_SELECT_ATTR_VAL', value: e.target.value })
  }
  const selectVariantValue = (id) => {
    return dispatch({ type: 'CHANGE_SELECT_ATTR_VARIANT_VAL', id })
  }
  const addProductPhotos = (e) => {
    const newProductPhotos = [];
    for(let i = 0; i < e.target.files.length; i++) {
      newProductPhotos.push(e.target.files[i]);
    }
    setProductPhotos(newProductPhotos);
  }
  const removeImage = (idx) => {
    const newProductPhotos = productPhotos.filter((el, i) => i !== idx);
    setProductPhotos(newProductPhotos);
  }
  const removeSize = (sizeId, variantId) => {
    return dispatch({ type: 'REMOVE_SIZE_IN_VARIANT', sizeId, variantId })
  }
  const addNewCat = (type) => {
    let action;
    switch (type) {
      case 'location':
        action = { type: 'ADD_CATEGORY_LOCATION', value: true }
        return dispatch(action)
      case 'material':
        action = { type: 'ADD_CATEGORY_MATERIAL', value: true }
        return dispatch(action)
      // case 'variant':
      //   action = { type: 'ADD_CATEGORY_VARIANT', value: true }
      //   return dispatch(action)
      // case 'color':
      //   action = { type: 'ADD_CATEGORY_COLOR', value: true }
      //   return dispatch(action)
      // case 'size':
      //   action = { type: 'ADD_CATEGORY_SIZE', value: true }
      //   return dispatch(action)
      default:
        return
    }
  }
  // const addCategpry = (type) => {
  //   let action;
  //   switch (type) {
  //     case 'location':
  //       action = { type: 'ADD_LOCATION_CATEGORY' }
  //       return dispatch(action)
  //     case 'material':
  //       action = { type: 'ADD_MATERIAL_CATEGORY' }
  //       return dispatch(action)
  //     case 'variant':
  //       action = { type: 'ADD_COLOR_VARIANT' }
  //       return dispatch(action)
  //     // case 'color':
  //     //   action = { type: 'ADD_COLOR_CATEGORY' }
  //     //   return dispatch(action)
  //     // case 'size':
  //     //   action = { type: 'ADD_SIZE_CATEGORY' }
  //     //   return dispatch(action)
  //     default:
  //       return
  //   }
  // }
  const removeBlock = (type) => {
    let action;
    switch (type) {
      case 'location':
        action = { type: 'CHANGE_TYPE_LOCATION', value: false }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_TYPE_MATERIAL', value: false }
        return dispatch(action)
      case 'variant':
        action = { type: 'CHANGE_TYPE_VARIANT', value: false }
        return dispatch(action)
      // case 'color':
      //   action = { type: 'CHANGE_TYPE_COLOR', value: false }
      //   return dispatch(action)
      // case 'size':
      //   action = { type: 'CHANGE_TYPE_SIZE', value: false }
      //   return dispatch(action)
      default:
        return
    }
  }
  const addAttr = () => {
    let action;
    switch (newProd.prodAttributes.selectAttributeValue) {
      case 'location':
        action = { type: 'CHANGE_TYPE_LOCATION', value: true }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_TYPE_MATERIAL', value: true }
        return dispatch(action)
      case 'variant':
        action = { type: 'CHANGE_TYPE_VARIANT', value: true }
        return dispatch(action)
      // case 'color':
      //   action = { type: 'CHANGE_TYPE_COLOR', value: true }
      //   return dispatch(action)
      // case 'size':
      //   action = { type: 'CHANGE_TYPE_SIZE', value: true }
      //   return dispatch(action)
      default:
        return
    }
  }
  const newCategoryValue = (e, prodAttr) => {
    let action;
    switch (prodAttr) {
      case 'location':
        action = { type: 'CHANGE_LOCATION_CATEGORY_VAL', value: e.target.value }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_MATERIAL_CATEGORY_VAL', value: e.target.value }
        return dispatch(action)
      // case 'variant':
      //   action = { type: 'CHANGE_VARIANT_CATEGORY_VAL', value: e.target.value }
      //   return dispatch(action)
      // case 'color':
      //   action = { type: 'CHANGE_COLOR_CATEGORY_VAL', value: e.target.value }
      //   return dispatch(action)
      // case 'size':
      //   action = { type: 'CHANGE_SIZE_CATEGORY_VAL', value: e.target.value }
      //   return dispatch(action)
      default:
        return
    }
  }

 
  const addProduct = () => {
    const checkedCategories = productCategories.filter(item => item.checked).map(item => item.id);
    const checkedMaterials = productMaterials.filter(item => item.checked).map(item => item.id);
    const checkedColors = productColors.filter(item => item.checked).map(item => item);
    const checkedSize = productSize.filter(item => item.checked).map(item => item)

    const formData = new FormData();
    const productObj = {
      name: productName,
      slug: productName.replaceAll(" ", "-"),
      short_desc: productDescription,
      price: price,
      sale_price: salePrice,
      code: productCode,
      stock: stockQuantity,
      in_stock: productStock,
      new: productNew,
    };
    for (const key of Object.keys(productPhotos)) {
      formData.append('imgCollection', productPhotos[key])
    }
    if(
      productName.trim() != '' && 
      checkedCategories.length && 
      productDescription.trim() != '' && 
      checkedMaterials.length && 
      price.trim() != '' && 
      checkedColors.length &&
      checkedSize.length &&
      productCode.trim() != '' &&
      productPhotos.length
      ) {
      formData.append('product', JSON.stringify(productObj))
      formData.append('productCategories', JSON.stringify(checkedCategories))
    formData.append('productMaterials', JSON.stringify(checkedMaterials))
    formData.append('productColors', JSON.stringify(checkedColors))
    formData.append('productSize', JSON.stringify(checkedSize))
      axios.post('http://localhost:8080/addProd', formData)
    .then(res => {
      setProducts(res.data.products);
      setProductName('');
      setProductDescription('');
      setProductCategories(productCategories.map(item => ({...item, checked: false}) ));
      setProductMaterials(productMaterials.map(item => ({...item, checked: false}) ));
      setProductColors(productColors.map(item => ({...item, checked: false})));
      setProductSize(productSize.map(item => ({...item, checked: false})));
      setProductPhotos([]);
      setPrice("");
      setSalePrice("");
      setProductStock("true");
      setStockQuantity("");
      setProductNew(false);
    })
    .catch(err => {
      console.log('errrrrrr', err)
    });
    setAddProd(!addProd);
    setValidated(true)
    
  } else {
      setValidated(false)
    }
  }

  const handleProductCategoryChange = (idx) => {
    const newProductCategories = [...productCategories];
    newProductCategories[idx].checked = !newProductCategories[idx].checked;
    setProductCategories(newProductCategories);
  }
  const handleProductMaterialsChange = (idx) => {
    const newProductMaterials = [...productMaterials];
    newProductMaterials[idx].checked = !newProductMaterials[idx].checked;
    setProductMaterials(newProductMaterials);
  }
  const handleProductColorsChange = (idx) => {
    const newProductColors = [...productColors];
    newProductColors[idx].checked = !newProductColors[idx].checked;
    setProductColors(newProductColors);
    console.log('newProductColors', newProductColors)
  }
  const handleProductSizeChange = (idx) => {
    const newProductSize = [...productSize];
    newProductSize[idx].checked = !newProductSize[idx].checked;
    setProductSize(newProductSize);
    console.log('newProductSize', newProductSize)
  }

  useEffect(() => {
    axios.get('http://localhost:8080/category', {headers: {"Access-Control-Allow-Origin": "*"}}).then(res => {
        if(res.status === 200) {
          setProductCategories(res.data.categories)
        }
    })
    .catch(err => {
      console.log('el', err)
    });
    
    axios.get('http://localhost:8080/materials', {headers: {"Access-Control-Allow-Origin": "*"}}).then(res => {
        if(res.status === 200) {
          setProductMaterials(res.data.materials)
        }
    })
    .catch(err => {
      console.log('elol', err)
    });
  }, [])
 

  return (<>
    <CModal
      size="xl"
      show={addProd}
      onClose={() => setAddProd(!addProd)}
      color="success"
    >
      <CModalHeader closeButton>
        <CModalTitle>Ավելացնել նոր ապրանք</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardBody>
              <CFormGroup>
                <CLabel htmlFor="product_name">Ապրանքի անունը</CLabel>
                <CInput id="product_name" placeholder="Մուտքագրեք ապրանքի անունը" onChange={(e) => setProductName(e.target.value)} value={productName} />
                {!validated && !productName  ? <span style={{color: 'red'}}>Մուտքագրեք ապրանքի անունը</span> : <></>}
                {/* <CLabel htmlFor="product_rating">Rating</CLabel>
                <CInput id="product_rating" type="number" placeholder="Enter product rating" onChange={(e) => newProdRating(e)} defaultValue={newProd.rating} /> */}
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="product_desc">Ապրանքի նկարագրությունը</CLabel>
                <CTextarea id="product_desc" placeholder="Մուտքագրեք ապրանքի նկարագրությունը" rows="3" onChange={(e) => setProductDescription(e.target.value)} value={productDescription}></CTextarea>
                {!validated && !productDescription  ? <span style={{color: 'red'}}>Մուտքագրեք ապրանքի նկարագրությունը</span> : <></>}
              </CFormGroup>
              <CFormGroup>
                {
                  productCategories.length ? <CFormGroup>
                  <CLabel>Ընտրեք ապրանքի կատեգորիաները</CLabel>
                </CFormGroup>: <span style={{color: '#EED202'}}>Ավելացրեք ապրանքի կատեգորիա <b>Product Categories</b> բաժնում</span>
                }
                <>
                {productCategories.map((el, i) => {
                  return (
                    <CFormGroup variant="custom-checkbox" inline key={i}>
                      <CInputCheckbox
                        custom id={'catrgory_' + el.id}
                        checked={el.checked}
                        onChange={() => handleProductCategoryChange(i)}
                        name={el.name}
                      />
                      <CLabel variant="custom-checkbox" htmlFor={'catrgory_' + el.id}>{el.name}
                      </CLabel>
                    </CFormGroup>
                  )
                })}
                {!validated && productCategories.filter(item => item.checked).length == 0  ? <p style={{color: 'red'}}>Ընտրեք նվազագույնը մեկ կատեգորիա</p> : <></>}

                </>
              </CFormGroup>

              <CFormGroup>
                {
                  productMaterials.length ? <CFormGroup>
                  <CLabel>Ընտրեք ապրանքի մատերիալը</CLabel>
                </CFormGroup>: <span style={{color: '#EED202'}}>Ավելացրեք ապրանքի մատերիալ <b>Product Materials</b> բաժնում</span>
                }
                {productMaterials.map((el, i) => {
                  return (
                    <CFormGroup variant="custom-checkbox" inline key={i}>
                      <CInputCheckbox
                        custom id={'material_' + el.id}
                        checked={el.checked}
                        onChange={() => handleProductMaterialsChange(i)}
                        name={el.name}
                      />
                      <CLabel variant="custom-checkbox" htmlFor={'material_' + el.id}>{el.name}
                      </CLabel>
                    </CFormGroup>
                  )
                })}
                 {!validated && productMaterials.filter(item => item.checked).length == 0  ? <p style={{color: 'red'}}>Ընտրեք նվազագույնը մեկ մատերիալ</p> : <></>}
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="product_photo" className="custom-file-upload">Ավելացնել նկար</CLabel>
                <CInput
                  id="product_photo"
                  type="file"
                  name="imgCollection"
                  onChange={(e) => addProductPhotos(e)}
                  multiple="multiple"
                  accept="image/png, image/jpeg"
                />
                {productPhotos.length ?
                  <p className="info-text">Նկարը հեռացնելու համար սեղմեք տվյալ նկարի վրա</p> : <></>
                }
              </CFormGroup>
              <div className="show-preview-images">
                {
                  productPhotos.length
                  ? productPhotos.map((el, i) => <CImg onClick={() => removeImage(i)} src={URL.createObjectURL(el)} className="img-thumbnail preview-img" alt="" key={i} />) : <></>
                }
              </div>
                 {!validated && !productPhotos.length  ? <p style={{color: 'red'}}>Ընտրեք նվազագույնը մեկ նկար</p> : <></>}
              <h5>Ապրանքի տվյալները</h5>
              <CNav variant="tabs" role="tablist">
                <CNavItem>
                  <CNavLink
                    // href="javascript:void(0);"
                    active={activeKey === 1}
                    onClick={() => setActiveKey(1)}
                  >
                    Ընդհանուր
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink
                    active={activeKey === 2}
                    onClick={() => setActiveKey(2)}
                  >
                    Կոդը և առկայությունը
                  </CNavLink>
                </CNavItem>
                {/* <CNavItem>
                  <CNavLink
                    active={activeKey === 3}
                    onClick={() => setActiveKey(3)}
                  >
                    Առաքումը
                  </CNavLink>
                </CNavItem> */}
                {/* <CNavItem>
                  <CNavLink
                    active={activeKey === 4}
                    onClick={() => setActiveKey(4)}
                  >
                    Linked Products
                  </CNavLink>
                </CNavItem> */}
                <CNavItem>
                  {/* <CNavLink
                    active={activeKey === 5}
                    onClick={() => setActiveKey(5)}
                  >
                    Այլ տվյալներ
                  </CNavLink> */}
                </CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane role="tabpanel" aria-labelledby="general-tab" active={activeKey === 1}>
                  <CFormGroup row className="my-0">
                    <CCol xs="6">
                      <CFormGroup>
                        <CLabel htmlFor="reg_price">Հիմնական գին ( ֏)</CLabel>
                        <CInput id="reg_price" placeholder="Գինը դրամով" onChange={(e) => setPrice(e.target.value)} value={price} />
                        {!validated && !price  ? <span style={{color: 'red'}}>Նշեք ապրանքի գինը</span> : <></>}
                      </CFormGroup>
                    </CCol>
                    <CCol xs="6">
                      <CFormGroup>
                        <CLabel htmlFor="sale_price">Զեղջված գին ( ֏)</CLabel>
                        <CInput id="sale_price" placeholder="Գինը դրամով" onChange={(e) => setSalePrice(e.target.value)} value={salePrice} />
                      </CFormGroup>
                    </CCol>
                    <CCol xs="12">
                      <CLabel>Ընտրել գույնը</CLabel>
                      {productColors.map((color, i) => {
                        return (
                          <CFormGroup variant="checkbox" key={color.id}>
                            <CInputCheckbox id={color.en_name} className="mb-3" checked={color.checked} onChange={() => handleProductColorsChange(i)} />
                            <CLabel htmlFor={color.en_name}>{color.name}</CLabel>
                          </CFormGroup>
                        )
                      })}
                      {!validated && productColors.filter(item => item.checked).length == 0  ? <p style={{color: 'red'}}>Ընտրեք գույնը</p> : <></>}
                    </CCol>
                    <hr className="my-2 w-100" />
                    <CCol xs="12">
                      <CLabel>Ընտրել չափսը</CLabel>
                      {productSize.map((size, i) => {
                        return (
                          <CFormGroup variant="checkbox" key={size.id}>
                            <CInputCheckbox id={size.name} className="mb-3" checked={size.checked} onChange={() => handleProductSizeChange(i)} />
                            <CLabel htmlFor={size.name}>{size.name}</CLabel>
                          </CFormGroup>
                        )
                      })}
                      {!validated && productSize.filter(item => item.checked).length == 0  ? <p style={{color: 'red'}}>Ընտրեք չափսը</p> : <></>}
                    </CCol>
                    <hr className="my-2 w-100" />
                    <CCol className='mt-4'>
                      <CFormGroup variant="checkbox">
                        <CInputCheckbox id="newProduct" className="mb-3" onChange={() => setProductNew(!productNew)} checked={productNew}/>
                        <CLabel htmlFor="newProduct">Նոր է</CLabel>
                      </CFormGroup> 
                    </CCol>
                  </CFormGroup>
                  {/* <CTabContent>
                    <CTabPane role="tabpanel" aria-labelledby="profile-tab" active={activeSchedule === 1}>
                      <CFormGroup row className="my-0" >
                        <CCol xs="6">
                          <CFormGroup>
                            <CLabel htmlFor="saleFrom">From</CLabel>
                            <CInput type="date" id="saleFrom" />
                          </CFormGroup>
                        </CCol>
                        <CCol xs="6">
                          <CFormGroup>
                            <CLabel htmlFor="saleTo">To</CLabel>
                            <CInput type="date" id="saleTo" />
                          </CFormGroup>
                        </CCol>
                      </CFormGroup>
                    </CTabPane>
                    <CNavLink
                      onClick={() => priceDates(activeSchedule)}
                    >{activeSchedule === 1 ? "Cancel" : "Schedule"}</CNavLink>
                  </CTabContent> */}
                </CTabPane>
                <CTabPane role="tabpanel" aria-labelledby="inventory-tab" active={activeKey === 2}>
                  <CFormGroup>
                    <CLabel htmlFor="prodCode">Կոդը</CLabel>
                    <CInput id="prodCode" onChange={(e) => setProductCode(e.target.value)} value={productCode}/>
                    {!validated && !productCode  ? <span style={{color: 'red'}}>Մուտքագրեք ապրանքի կոդը</span> : <></>}
                  </CFormGroup>
                  <CTabContent>
                      <CFormGroup>
                        <CLabel htmlFor="_stock_status">Առկայությունը</CLabel>
                        <CSelect custom id="_stock_status" name="_stock_status" onChange={(e) => setProductStock(e.target.value)} value={productStock}>
                          <option value="true">Առկա է</option>
                          <option value="false">Առկա չէ</option>
                        </CSelect>
                      </CFormGroup>
                      {productStock === "true" ? 
                      <CFormGroup variant="checkbox">
                        <CInputCheckbox className="mb-3" id="stockManage" onChange={() => setManageStock(!manageStock)} checked={manageStock} />
                        <CLabel htmlFor="stockManage">Կառավարե՞լ պահեստը</CLabel>
                      </CFormGroup> : <></>
                      }
                      
                      {manageStock && productStock === "true" ?
                        <CFormGroup>
                          <CLabel htmlFor="stockQuantity">Քանակը պահեստում</CLabel>
                          <CInput id="stockQuantity" type="number" onChange={(e) => setStockQuantity(e.target.value)} value={stockQuantity} />
                        </CFormGroup> : <></>
                      }
                      {/* <CFormGroup>
                        <CLabel htmlFor="_backorders">Allow backorders?</CLabel>
                        <CSelect custom id="_backorders" name="_backorders">
                          <option value="no">Do not allow</option>
                          <option value="notify">Allow, but notify customer</option>
                          <option value="yes">Allow</option>
                        </CSelect>
                      </CFormGroup> */}
                      {/* <CFormGroup>
                        <CLabel htmlFor="lowStock">Low stock threshold</CLabel>
                        <CInput id="lowStock" type="number" placeholder="2" />
                      </CFormGroup> */}
                  </CTabContent>
                  {/* <CFormGroup variant="checkbox">
                    <CInputCheckbox className="mb-3" id="individuallySold" />
                    <CLabel htmlFor="individuallySold">Sold individually</CLabel>
                    <p>Enable this to only allow one of this item to be bought in a single order</p>
                  </CFormGroup> */}
                </CTabPane>
                {/* <CTabPane role="tabpanel" aria-labelledby="contact-tab" active={activeKey === 3}>
                  <CFormGroup>
                    <CLabel htmlFor="weight">Weight (g)</CLabel>
                    <CInput id="weight" placeholder="0" onChange={e => newProdWeight(e)} defaultValue={newProd.weight} />
                  </CFormGroup>
                  <CLabel>Dimensions (cm)</CLabel>
                  <CFormGroup row className="my-0" >
                    <CCol xs="4">
                      <CFormGroup>
                        <CInput id="dimensionsLength" placeholder="Length" onChange={e => newProdDimensionsLength(e)} defaultValue={newProd.dimensions.length} />
                      </CFormGroup>
                    </CCol>
                    <CCol xs="4">
                      <CFormGroup>
                        <CInput id="dimensionsWidth" placeholder="Width" onChange={e => newProdDimensionsWidth(e)} defaultValue={newProd.dimensions.width} />
                      </CFormGroup>
                    </CCol>
                    <CCol xs="4">
                      <CFormGroup>
                        <CInput id="dimensionsHeight" placeholder="Height" onChange={e => newProdDimensionsHeight(e)} defaultValue={newProd.dimensions.height} />
                      </CFormGroup>
                    </CCol>
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="product_shipping_class">Shipping class</CLabel>
                    <CSelect custom name="product_shipping_class" id="product_shipping_class">
                      <option value="-1">No shipping class</option>
                    </CSelect>
                  </CFormGroup>
                </CTabPane> */}
                <CTabPane role="tabpanel" aria-labelledby="contact-tab" active={activeKey === 4}>
                  <CFormGroup>
                    <CLabel htmlFor="upsells">Upsells</CLabel>
                    <CInput id="upsells" placeholder="Search for a product…" />
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="cross_sells">Cross-sells</CLabel>
                    <CInput id="cross_sells" placeholder="Search for a product…" />
                  </CFormGroup>
                </CTabPane>
                <CTabPane role="tabpanel" aria-labelledby="contact-tab" active={activeKey === 5}>
                  <CFormGroup row className="my-1" >
                    <CCol xs="10">
                      <CSelect custom name="attribute_taxonomy" onChange={(e) => selectValue(e)} value={newProd.prodAttributes.selectAttributeValue}>
                        <option value="">Ապրանքի անհատական հատկանիշ</option>
                        <option value="material" disabled={selectDisableType.material}>Մատերիալ</option>
                        <option value="variant" disabled={selectDisableType.variant} >Գույնը</option>
                        {/* <option value="location" disabled={selectDisableType.location}>Արտադրության վայր</option> */}
                        {/* <option value="color" disabled={selectDisableType.color}>Գույն</option>
                        <option value="size" disabled={selectDisableType.size}>Չափս</option> */}
                      </CSelect>
                    </CCol>
                    <CCol xs="2">
                      <CButton color="success"
                        variant="outline"
                        className="w-100"
                        onClick={() => addAttr()}
                      >Ավելացնել</CButton>
                    </CCol>
                  </CFormGroup>
                  {/* {
                    // newProd.prodAttributes.location,
                    // newProd.prodAttributes.material,
                    newProd.prodAttributes.location.addAttr && <ProdAttrLocation
                      prod_attr={newProd.prodAttributes.location}
                      addNewCat={addNewCat}
                      val="create"
                      removeBlock={removeBlock}
                      newCategoryValue={newCategoryValue}
                      addCategpry={addCategpry}
                    />
                  } */}
                  {/* {
                    newProd.prodAttributes.material.addAttr && <ProdAttrMaterial
                      prod_attr={newProd.prodAttributes.material}
                      addNewCat={addNewCat}
                      val="create"
                      removeBlock={removeBlock}
                      newCategoryValue={newCategoryValue}
                      addCategpry={addCategpry}
                    />
                  } */}
                  {
                    newProd.prodAttributes.variant.addAttr
                    && <>
                      {newProd.prodAttributes.variant.colors.map((color, i) =>
                        <CFormGroup row className="my-1" key={i}>
                          <CCol xs="2">
                            <CButton block color={color.selected ? "success" : "light"} onClick={() => selectVariantValue(color.id)}>{color.title}</CButton>
                          </CCol>
                          <CCol xs="3">
                            {color.selected
                              && <ProdAttrVariant
                                val={color.title}
                                sizes={newProd.prodAttributes.variant.sizes}
                                colorId={color.id}
                              />}
                          </CCol>
                          <CCol xs="7">
                            <CRow className="align-items-center">
                              {newProd.prodAttributes.variants.map(variants => variants.id === +color.id
                                && variants.size.map(size =>
                                  <CButtonGroup>
                                    <CButton onClick={() => removeSize(size.id, variants.id)} block color="info">{size.name}</CButton>
                                  </CButtonGroup>
                                ))}
                            </CRow>
                          </CCol>
                        </CFormGroup>
                      )}
                      <CCol md="12">
                        {/* <CButton shape="pill" color="info" className="w-50" onClick={() => addNewCat(props.prod_attr.name)}>
                          Ավելացնել նորը
                        </CButton> */}
                        <CButton shape="pill" color="danger" className="w-100" onClick={() => removeBlock(newProd.prodAttributes.variant.name)}>
                          Հեռացնել
                        </CButton>
                      </CCol>
                    </>
                  }
                </CTabPane>
              </CTabContent>
              {/* <h5>Product Config</h5>
              <CFormGroup row className="my-0">
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="snsnitan_product_sidebar">Product Sidebar</CLabel>
                    <CSelect custom id="snsnitan_product_sidebar" name="snsnitan_product_sidebar">
                      <option value selected="selected">Default</option>
                      <option value="2">No</option>
                      <option value="3">Yes</option>
                    </CSelect>
                    <p>Product page with Sidebar. Select "Default" to use in Theme Options.</p>
                  </CFormGroup>
                </CCol>
                <CCol xs="6">
                  <CFormGroup>
                    <CLabel htmlFor="snsnitan_woo_zoomtype">Cloud Zoom</CLabel>
                    <CSelect custom id="snsnitan_woo_zoomtype" name="snsnitan_woo_zoomtype">
                      <option value selected="selected">Default</option>
                      <option value="window">Window</option>
                      <option value="lens">lens</option>
                      <option value="inner">Inner</option>
                    </CSelect>
                  </CFormGroup>
                </CCol>
              </CFormGroup> */}
              {/* <CFormGroup>
                <CLabel htmlFor="videoURL">Product Video</CLabel>
                <CInput id="videoURL" placeholder="Enter your video url(youtube, video)" />
              </CFormGroup> */}
            </CCardBody>
          </CCard>
        </CCol>
      </CModalBody>
      <CModalFooter>
        <CButton color="danger" onClick={() => setAddProd(!addProd)}>Cancel</CButton>{' '}
        <CButton color="success" onClick={() => addProduct()}>Add Product</CButton>
      </CModalFooter>
    </CModal>
  </>)
}

export default AddOrEditProductModal