<>
  <CFormGroup row className="align-items-center" key={j}>
    <CCol md="2">
      <CLabel>{prod_attr.title}</CLabel>
    </CCol>
    <CCol md="6">
      {
        prod_attr.categories_radio
          ? prod_attr.categories_radio.map((el, i) => {
            return (
              <CFormGroup variant="custom-radio" inline key={i}>
                <CInputRadio
                  custom id={prod_attr.name + '_' + i}
                  name={prod_attr.name}
                  checked={el.checked}
                  onChange={() => newProdAttributeCategory(prod_attr.name, el.id)}
                />
                <CLabel variant="custom-checkbox" htmlFor={prod_attr.name + '_' + i}>{el.name}</CLabel>
              </CFormGroup>
            )
          })
          : prod_attr.categories.map((el, i) => {
            return (
              <CFormGroup variant="custom-checkbox" inline key={i}>
                <CInputCheckbox
                  custom id={'catrgory_' + prod_attr.name + '_' + el.id}
                  name={el.name}
                  checked={el.checked}
                  onChange={() => newProdAttributeCategory(prod_attr.name, el.id)}
                />
                <CLabel variant="custom-checkbox" htmlFor={'catrgory_' + prod_attr.name + '_' + el.id}>{el.name}</CLabel>
              </CFormGroup>
            )
          })
      }
    </CCol>
    <CCol md="4">
      <CButton shape="pill" color="info" className="w-50" onClick={() => addNewCat(prod_attr.name)}>
        Ավելացնել նորը
      </CButton>
      <CButton shape="pill" color="danger" className="w-50" onClick={() => removeBlock(prod_attr.name)}>
        Հեռացնել
      </CButton>
    </CCol>
  </CFormGroup>
  {prod_attr.addCategory
    && <CFormGroup row>
      <CCol md="10">
        <CInput defaultValue={prod_attr.addNew} onChange={(e) => newCategoryValue(e, prod_attr.name)} />
      </CCol>
      <CCol md="2">
        <CButton block variant="outline" color="info" onClick={() => addCategpry(prod_attr.name)}>+</CButton>
      </CCol>
    </CFormGroup>}
</>