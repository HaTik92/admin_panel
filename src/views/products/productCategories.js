import React, { useState, useEffect } from 'react';
import AddCategoriesModal from './addCategoriesModal';
import axios from 'axios'

import {
    CBadge,
    CCard,
    CCardBody,
    CCardHeader,
    CCol,
    CDataTable,
    CRow,
    CButton,
    CModal,
    CModalHeader,
    CModalTitle,
    CModalFooter,
} from '@coreui/react'

  const fields = [
    'id',
    'name',
    {
      key: 'show_details',
      label: '',
      sorter: false,
      filter: false
    }
  ];
  const getBadge = status => {
    switch (status) {
      case 'instock': return 'success'
      case 'onbackorder': return 'warning'
      case 'outofstock': return 'danger'
      default: return 'primary'
    }
  }

 
  const ProductCategories = () => {
    const [details_delete, setDetailsDelete] = useState([])
    const [addProd, setAddProd] = useState(false)
    const [productName, setProductName] = useState('');
    const [validated, setValidated] = useState(true);
    const [categories, setCategories] = useState([]);

    useEffect(() => {
        axios.get('http://localhost:8080/category', {headers: {"Access-Control-Allow-Origin": "*"}}).then(res => {
            if(res.status === 200) {
                setCategories(res.data.categories)
              }
      })
      .catch(err => {
        console.log('errrrrrr', err)
      });
    }, [])

    const addCategory = () => {
        axios.post('http://localhost:8080/addCategory', {name: productName, slug: productName.replaceAll(" ", "-")})
        .then(res => {
          setCategories(res.data.categories);
          setProductName('');
        })
        .catch(err => {
          console.log('errrrrrr', err)
        });
        setAddProd(!addProd) 
    }

    const modalDetailsDelete = (index = '') => {
      setDetailsDelete(index)
    }

    const openModalAddProd = () => {
      setAddProd(!addProd)
    }

    const deleteCategory = (id) => {
      axios.post('http://localhost:8080/deleteCategory', {id: id})
      .then(res => {
        modalDetailsDelete('');
        setCategories(res.data.categories);
      })
      .catch(err => {
        console.log('errrrrrr', err)
      });
    }

    return (
    <CRow>
      <CCol>
        <CCard>
          <CCardHeader>
            Բոլոր տեսակները
            <CButton className="float-right" size="sm" color="dark" onClick={() => openModalAddProd()}>Add Categories</CButton>
            <AddCategoriesModal 
              addProd={addProd} 
              setAddProd={setAddProd} 
              addCategory={addCategory}
              productName={productName}
              setProductName={setProductName}
            />
          </CCardHeader>
          <CCardBody>
            <CDataTable
                items={categories}
                fields={fields}
                sorter
                hover
                striped
                bordered
                size="md"
                itemsPerPage={10}
                pagination
                scopedSlots={
                    {
                      'stock':
                        (item) => (
                          <td>
                            <CBadge color={getBadge(item.status.value)}>
                              {item.status.title}
                            </CBadge>
                          </td>
                        ),
                    //   'photo':
                    //     (item) => (
                    //       <td>
                    //         <CImg
                    //           src={item.pictures.length > 0 ? item.pictures[0].url : 'product_images/default.png'}
                    //           width="50"
                    //           alt=""
                    //         />
                    //         {/* <img src={item.pictures.length > 0 ? item.pictures[0].url : ''} width="50" alt="" /> */}
                    //       </td>
                    //     )
                      'show_details':
                        (item, index) => {
                          return (
                            <td>
                              <CButton color="danger"
                                onClick={() => { modalDetailsDelete(index) }}
                                className={`mr-1, ${item.id}`}>Ջնջել</CButton>
                            </td>
                          )
                        },
                      'details':
                        (item, index) => {
                          return (<>
                            {/* <EditProductModal
                              item={item}
                            //   details_edit={details_edit}
                            //   index={index}
                            //   modalDetailsEdit={modalDetailsEdit}
                            /> */}
                            <CModal
                              show={details_delete === index}
                              onClose={() => modalDetailsDelete('')}
                              color="warning"
                            >
                              <CModalHeader closeButton>
                                <CModalTitle>Դուք համոզվա՞ծ եք</CModalTitle>
                              </CModalHeader>
                              <CModalFooter>
                                <CButton color="secondary" onClick={() => modalDetailsDelete('')}>Չեղարկել</CButton>{' '}
                                <CButton color="danger" onClick={() => deleteCategory(item.id)}>Այո</CButton>
                              </CModalFooter>
                            </CModal>
                          </>
                          )
                        }
                    }
                  }
            />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
    )
}

export default ProductCategories