import React, { useState } from 'react';
import ProdAttrLocation from './prodAttrLocation';
import ProdAttrMaterial from './prodAttrMaterial';
import ProdAttrVariant from './prodAttrVariant';
import { useDispatch, useSelector } from 'react-redux';

import {
  CCard,
  CCardBody,
  CCol,
  CRow,
  CButton,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CFormGroup,
  CLabel,
  CInput,
  CSelect,
  CNav,
  CNavItem,
  CNavLink,
  CTabContent,
  CTabPane,
  CInputCheckbox,
  CButtonGroup,
  CImg
} from '@coreui/react';

const EditProductModal = (props) => {
  // const oneProd = useSelector(state => state.products.filter(el => el.id === props.id)[0])
  const newProd = useSelector(state => state.product.newOrEditProduct)
  const dispatch = useDispatch()
  const {editProduct, setEditProduct, item , index , modalDetailsEdit} = props
  // const setEditProduct = props
  const [activeKey, setActiveKey] = useState(1)
  const [activeSchedule, setActiveSchedule] = useState(0)
  const [activeStockManage, setActiveStockManage] = useState(1)

  const priceDates = (key) => {
    key === 1 ? key = 0 : key = 1
    setActiveSchedule(key)
  }
  const stockManage = (key) => {
    key === 1 ? key = 2 : key = 1
    setActiveStockManage(key)
  }


  let selectDisableType = {
    location: newProd.prodAttributes.location.addAttr,
    material: newProd.prodAttributes.material.addAttr,
    variant: newProd.prodAttributes.variant.addAttr,
  }
  const newProdName = (e) => {
    return dispatch({ type: 'NEW_PRODUCT_NAME', value: e.target.value })
  }
  const newProdRegularPrice = (e) => {
    return dispatch({ type: 'NEW_PRODUCT_REGULAR_PRICE', value: e.target.value })
  }
  const newProdSalePrice = (e) => {
    return dispatch({ type: 'NEW_PRODUCT_SALE_PRICE', value: e.target.value })
  }
  const newProdInStock = (e) => {
    return dispatch({ type: 'NEW_PRODUCT_IN_STOCK', value: e.target.value })
  }
  const newProdCategory = (categoryId) => {
    return dispatch({ type: 'NEW_PRODUCT_CATEGORY', value: categoryId })
  }
  const selectValue = (e) => {
    return dispatch({ type: 'CHANGE_SELECT_ATTR_VAL', value: e.target.value })
  }
  const selectVariantValue = (id) => {
    return dispatch({ type: 'CHANGE_SELECT_ATTR_VARIANT_VAL', id })
  }
  const addPhoto = (e) => {
    if (e.target.files) {
      let img;
      img = e.target.files[0]
      return dispatch({ type: 'NEW_PRODUCT_UPLOAD_PHOTO', url: img })
    }
  }
  const removeImage = (url) => {
    return dispatch({ type: 'REMOVE_PRODUCT_IMAGE', url })
  }
  const removeSize = (sizeId, variantId) => {
    return dispatch({ type: 'REMOVE_SIZE_IN_VARIANT', sizeId, variantId })
  }
  const addNewCat = (type) => {
    let action;
    switch (type) {
      case 'location':
        action = { type: 'ADD_CATEGORY_LOCATION', value: true }
        return dispatch(action)
      case 'material':
        action = { type: 'ADD_CATEGORY_MATERIAL', value: true }
        return dispatch(action)
      default:
        return
    }
  }
  const addCategpry = (type) => {
    let action;
    switch (type) {
      case 'location':
        action = { type: 'ADD_LOCATION_CATEGORY' }
        return dispatch(action)
      case 'material':
        action = { type: 'ADD_MATERIAL_CATEGORY' }
        return dispatch(action)
      case 'variant':
        action = { type: 'ADD_COLOR_VARIANT' }
        return dispatch(action)
      default:
        return
    }
  }
  const removeBlock = (type) => {
    let action;
    switch (type) {
      case 'location':
        action = { type: 'CHANGE_TYPE_LOCATION', value: false }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_TYPE_MATERIAL', value: false }
        return dispatch(action)
      case 'variant':
        action = { type: 'CHANGE_TYPE_VARIANT', value: false }
        return dispatch(action)
      default:
        return
    }
  }
  const addAttr = () => {
    let action;
    switch (newProd.prodAttributes.selectAttributeValue) {
      case 'location':
        action = { type: 'CHANGE_TYPE_LOCATION', value: true }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_TYPE_MATERIAL', value: true }
        return dispatch(action)
      case 'variant':
        action = { type: 'CHANGE_TYPE_VARIANT', value: true }
        return dispatch(action)
      default:
        return
    }
  }
  const newCategoryValue = (e, prodAttr) => {
    let action;
    switch (prodAttr) {
      case 'location':
        action = { type: 'CHANGE_LOCATION_CATEGORY_VAL', value: e.target.value }
        return dispatch(action)
      case 'material':
        action = { type: 'CHANGE_MATERIAL_CATEGORY_VAL', value: e.target.value }
        return dispatch(action)
      default:
        return
    }
  }
  const saveChanges = (id) => {
    props.modalDetailsEdit('')
    return dispatch({ type: 'SAVE_CHANGES', id })
  }


  return (<>
    <CModal
      size="xl"
      show={editProduct}
      onClose={() => setEditProduct(!editProduct)}
      // show={props.details_edit === props.index}
      // onClose={() => props.modalDetailsEdit('')}
      color="success"
    >
      <CModalHeader closeButton>
        <CModalTitle>Փոփոխել ապրանքը</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardBody>
              <CFormGroup>
                <CLabel htmlFor="product_name">Ապրանքի անունը</CLabel>
                <CInput id="product_name" placeholder="մուտքագրեք ապրանքի անունը" onChange={(e) => newProdName(e)} defaultValue={props.item.name} />
              </CFormGroup>
              <CFormGroup>
                <CFormGroup>
                  <CLabel>Ընտրեք ապրանքի կատեգորիաները</CLabel>
                </CFormGroup>
                {newProd.productsCategories.map((el, i) => {
                  return (
                    <CFormGroup variant="custom-checkbox" inline key={i}>
                      <CInputCheckbox
                        custom id={'edit_catrgory_' + el.id}
                        checked={el.checked}
                        onChange={() => newProdCategory(el.id)}
                        name={el.name}
                      />
                      <CLabel variant="custom-checkbox" htmlFor={'edit_catrgory_' + el.id}>{el.name}
                      </CLabel>
                    </CFormGroup>
                  )
                })}
              </CFormGroup>
              <CFormGroup>
                <CLabel htmlFor="product_photo">Ավելացնել նկար</CLabel>
                <CInput
                  id="product_photo"
                  type="file"
                  onChange={(e) => addPhoto(e)}
                  multiple="multiple" accept="image/png, image/jpeg"
                  defaultValue={newProd.prodPhotoInputValue} />
                <p>նկարը հեռացնելու համար սեղմեք տվյալ նկարի վրա</p>
              </CFormGroup>
              {
                newProd.pictures
                && newProd.pictures.map((el, i) => <CImg src={el.url} onClick={() => removeImage(el.url)} className="img-thumbnail" width='200' alt="" key={i} />)
              }
              <hr />
              <h5>Ապրանքի տվյալները</h5>
              <CNav variant="tabs" role="tablist">
                <CNavItem>
                  <CNavLink
                    // href="javascript:void(0);"
                    active={activeKey === 1}
                    onClick={() => setActiveKey(1)}
                  >
                    Ընդհանուր
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  <CNavLink
                    active={activeKey === 2}
                    onClick={() => setActiveKey(2)}
                  >
                    Կոդը և առկայությունը
                  </CNavLink>
                </CNavItem>
                <CNavItem>
                  {/* <CNavLink
                    active={activeKey === 5}
                    onClick={() => setActiveKey(5)}
                  >
                    Այլ տվյալներ
                  </CNavLink> */}
                </CNavItem>
              </CNav>
              <CTabContent>
                <CTabPane role="tabpanel" aria-labelledby="general-tab" active={activeKey === 1}>
                  <CFormGroup row className="my-0">
                    <CCol xs="6">
                      <CFormGroup>
                        <CLabel htmlFor="reg_price">Հիմնական գին ( ֏)</CLabel>
                        <CInput id="reg_price" placeholder="գինը դրամով" onChange={(e) => newProdRegularPrice(e)} defaultValue={props.item.price} />
                      </CFormGroup>
                    </CCol>
                    <CCol xs="6">
                      <CFormGroup>
                        <CLabel htmlFor="sale_price">Զեղջված գին ( ֏)</CLabel>
                        <CInput id="sale_price" placeholder="գինը դրամով" onChange={(e) => newProdSalePrice(e)} defaultValue={props.item.sale_price} />
                      </CFormGroup>
                    </CCol>
                  </CFormGroup>
                  <CTabContent>
                    <CTabPane role="tabpanel" aria-labelledby="profile-tab" active={activeSchedule === 1}>
                      <CFormGroup row className="my-0" >
                        <CCol xs="6">
                          <CFormGroup>
                            <CLabel htmlFor="saleFrom">From</CLabel>
                            <CInput type="date" id="saleFrom" />
                          </CFormGroup>
                        </CCol>
                        <CCol xs="6">
                          <CFormGroup>
                            <CLabel htmlFor="saleTo">To</CLabel>
                            <CInput type="date" id="saleTo" />
                          </CFormGroup>
                        </CCol>
                      </CFormGroup>
                    </CTabPane>
                    <CNavLink
                      onClick={() => priceDates(activeSchedule)}
                    >{activeSchedule === 1 ? "Cancel" : "Schedule"}</CNavLink>
                  </CTabContent>
                </CTabPane>
                <CTabPane role="tabpanel" aria-labelledby="inventory-tab" active={activeKey === 2}>
                  <CFormGroup>
                    <CLabel htmlFor="prodCode">Code</CLabel>
                    <CInput id="prodCode" />
                  </CFormGroup>
                  <CFormGroup variant="checkbox">
                    <CInputCheckbox className="mb-3" id="stockManage" onChange={() => stockManage(activeStockManage)} />
                    <CLabel htmlFor="stockManage">Manage stock?</CLabel>
                    <p>Enable stock management at product level</p>
                  </CFormGroup>
                  <CTabContent>
                    <CTabPane role="tabpanel" aria-labelledby="profile-tab" active={activeStockManage === 1}>
                      <CFormGroup>
                        <CLabel htmlFor="_stock_status">Առկայությունը</CLabel>
                        <CSelect custom id="_stock_status" name="_stock_status" onChange={(e) => newProdInStock(e)} defaultValue={props.item.status.value}>
                          <option value="" disabled>պահեստում</option>
                          <option value="instock">առկա է պահեստում</option>
                          <option value="outofstock">առկա չէ պահեստում</option>
                        </CSelect>
                      </CFormGroup>
                    </CTabPane>
                    <CTabPane role="tabpanel" aria-labelledby="profile-tab" active={activeStockManage === 2}>
                      <CFormGroup>
                        <CLabel htmlFor="stockQuantity">Stock quantity</CLabel>
                        <CInput id="stockQuantity" type="number" />
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel htmlFor="_backorders">Allow backorders?</CLabel>
                        <CSelect custom id="_backorders" name="_backorders">
                          <option value="no">Do not allow</option>
                          <option value="notify">Allow, but notify customer</option>
                          <option value="yes">Allow</option>
                        </CSelect>
                      </CFormGroup>
                      <CFormGroup>
                        <CLabel htmlFor="lowStock">Low stock threshold</CLabel>
                        <CInput id="lowStock" type="number" placeholder="2" />
                      </CFormGroup>
                    </CTabPane>
                  </CTabContent>
                </CTabPane>
                <CTabPane role="tabpanel" aria-labelledby="contact-tab" active={activeKey === 4}>
                  <CFormGroup>
                    <CLabel htmlFor="upsells">Upsells</CLabel>
                    <CInput id="upsells" placeholder="Search for a product…" />
                  </CFormGroup>
                  <CFormGroup>
                    <CLabel htmlFor="cross_sells">Cross-sells</CLabel>
                    <CInput id="cross_sells" placeholder="Search for a product…" />
                  </CFormGroup>
                </CTabPane>
                <CTabPane role="tabpanel" aria-labelledby="contact-tab" active={activeKey === 5}>
                  <CFormGroup row className="my-1" >
                    <CCol xs="10">
                      <CSelect custom name="attribute_taxonomy" onChange={(e) => selectValue(e)} value={newProd.prodAttributes.selectAttributeValue}>
                        <option value="">Ապրանքի անհատական հատկանիշ</option>
                        <option value="location" disabled={selectDisableType.location}>Արտադրության վայր</option>
                        <option value="material" disabled={selectDisableType.material}>Մատերիալ</option>
                        <option value="variant" disabled={selectDisableType.variant}>Տարբերակներ</option>
                      </CSelect>
                    </CCol>
                    <CCol xs="2">
                      <CButton color="success"
                        variant="outline"
                        className="w-100"
                        onClick={() => addAttr()}
                      >Ավելացնել</CButton>
                    </CCol>
                  </CFormGroup>
                  {
                    newProd.prodAttributes.location.addAttr && <ProdAttrLocation
                      prod_attr={newProd.prodAttributes.location}
                      val="edit"
                      addNewCat={addNewCat}
                      removeBlock={removeBlock}
                      newCategoryValue={newCategoryValue}
                      addCategpry={addCategpry}
                    />
                  }
                  {
                    newProd.prodAttributes.material.addAttr && <ProdAttrMaterial
                      prod_attr={newProd.prodAttributes.material}
                      val="edit"
                      addNewCat={addNewCat}
                      removeBlock={removeBlock}
                      newCategoryValue={newCategoryValue}
                      addCategpry={addCategpry}
                    />
                  }
                  {
                    newProd.prodAttributes.variant.addAttr
                    && <>
                      {newProd.prodAttributes.variant.colors.map((color, i) =>
                        <CFormGroup row className="my-1" key={i}>
                          <CCol xs="2">
                            <CButton block color={color.selected ? "success" : "light"} onClick={() => selectVariantValue(color.id)}>{color.title}</CButton>
                          </CCol>
                          <CCol xs="3">
                            {color.selected
                              && <ProdAttrVariant
                                val={color.title}
                                sizes={newProd.prodAttributes.variant.sizes}
                                colorId={color.id}
                              />}
                          </CCol>
                          <CCol xs="7">
                            <CRow className="align-items-center">
                              {newProd.prodAttributes.variants.map(variants => variants.id === +color.id
                                && variants.size.map(size =>
                                  <CButtonGroup>
                                    <CButton onClick={() => removeSize(size.id, variants.id)} block color="info">{size.name}</CButton>
                                  </CButtonGroup>
                                ))}
                            </CRow>
                          </CCol>
                        </CFormGroup>
                      )}
                      <CCol md="12">
                        <CButton shape="pill" color="danger" className="w-100" onClick={() => removeBlock(newProd.prodAttributes.variant.name)}>
                          Հեռացնել
                        </CButton>
                      </CCol>
                    </>
                  }
                </CTabPane>
              </CTabContent>
              <hr />
            </CCardBody>
          </CCard>
        </CCol>
      </CModalBody>
      <CModalFooter>
        <CButton color="danger" onClick={() => props.modalDetailsEdit('')}>Չեղարկել</CButton>{' '}
        <CButton color="success" onClick={() => saveChanges(props.item.id)}>Պահպանել փոփոխությունները</CButton>
      </CModalFooter>
    </CModal>
  </>)
}

export default EditProductModal