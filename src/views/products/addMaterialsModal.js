import React, { useEffect, useState } from 'react';
import './Products.scss';

import {
  CCard,
  CCardBody,
  CCol,
  CButton,
  CModal,
  CModalBody,
  CModalHeader,
  CModalTitle,
  CModalFooter,
  CFormGroup,
  CLabel,
  CInput
} from '@coreui/react';
import axios from 'axios';



const AddMaterialsModal = (props) => {
  const {showAddMaterialPopup, setShowAddMaterialPopup, addCategory, materialName, setMaterialName} = props;
  const [validated, setValidated] = useState(true)

  const  hendleClickAddMaterial = () => {
      if(materialName != '') {
        addCategory() 
        setValidated(true)
      } else {
        setValidated(false)
      }
  }

  return (<>
    <CModal
      size="xl"
      show={showAddMaterialPopup}
      onClose={() => setShowAddMaterialPopup(!showAddMaterialPopup)}
      color="success"
    >
      <CModalHeader closeButton>
        <CModalTitle>Ավելացնել նոր մատերիալ</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CCol xs="12" sm="12">
          <CCard>
            <CCardBody>
              <CFormGroup>
                <CLabel htmlFor="producta_name">Մատերիալի անունը</CLabel>
                <CInput id="producta_name" placeholder="Մուտքագրեք մատերիալի անունը" onChange={(e) => setMaterialName(e.target.value)} value={materialName} />
                {!validated && !materialName  ? <span style={{color: 'red'}}>Մուտքագրեք մատերիալի անունը</span> : <></>}

              </CFormGroup>
            </CCardBody>
          </CCard>
        </CCol>
      </CModalBody>
      <CModalFooter>
        <CButton color="danger" onClick={() => setShowAddMaterialPopup(!showAddMaterialPopup)}>Չեղարկել</CButton>{' '}
        <CButton color="success" onClick={() => hendleClickAddMaterial()}>Ավելացնել</CButton>
      </CModalFooter>
    </CModal>
  </>)
}

export default AddMaterialsModal