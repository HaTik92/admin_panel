// import { combineReducers, createStore } from 'redux'
// import productsReducer from './redux/products-reducer'
// import TheHeaderReducer from './redux/TheHeaderReducer';




import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import rootReducer from './reducers';
import thunk from 'redux-thunk';


const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(thunk)
));
window.store = store

export default store;