const SET = "SET";

const initialState = {
  sidebarShow: 'responsive'
}
const showNavReducer = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    default:
      return state
  }
}


export default showNavReducer;