


const initialState = {
  admines: [
    { id: 1, username: 'admin1', password: 123456789 },
    { id: 2, username: 'admin2', password: 123456789 },
    { id: 3, username: 'admin3', password: 123456789 },
    { id: 4, username: 'admin4', password: 123456789 },
  ],
  loginAdmin: {
    username: '',
    password: '',
  },
  isAuth: false
}
// CHECK_PASSWORD
const loginReducer = (state = initialState, { type, ...rest }) => {
  console.log(rest);
  switch (type) {
    case 'CHECK_LOGIN':
      return {
        ...state,
        loginAdmin: {
          ...state.loginAdmin,
          username: rest.values.username,
          password: rest.values.password
        },
        isAuth: state.admines.some(admin => admin.username == rest.values.username && admin.password == rest.values.password)
      }
    case 'IS_AUTH':
      return {
        ...state,
        isAuth: rest.value
      }
    default:
      return state
  }
}


export default loginReducer;