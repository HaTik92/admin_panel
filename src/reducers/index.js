import { combineReducers } from "redux";
import addOrChangeProductReducer from "./addProd";
import loginReducer from "./loginReducer";
import showNavReducer from "./showNavReducer";


const rootReducer = combineReducers({
  nav: showNavReducer,
  product: addOrChangeProductReducer,
  login: loginReducer,
});

export default rootReducer;