import { createStore } from 'redux'
import usersData from './views/users/UsersData';

const initialState = {
  admines: [
    { id: 1, login: 'admin1', password: 123456789 },
    { id: 2, login: 'admin2', password: 123456789 },
    { id: 3, login: 'admin3', password: 123456789 },
    { id: 4, login: 'admin4', password: 123456789 },
  ],
  variantshow: true,
  sidebarShow: 'responsive',
  addNewCategory: "",
  products: [
    {
      id: 1,
      photo: "",
      code: 3564,
      name: "կանացի սպորտային կոշիկ՝ կաշվից",
      price: 24000,
      salePrice: 12000,
      ratings: 5,
      status: {
        title: "առկա չէ պահեստում",
        value: "outofstock"
      },
      weight: 700,
      dimensions: {
        length: 12,
        width: 22,
        height: 33
      },
      category: [
        { id: 2, name: "Բոթասներ", slug: "բոթասներ" },
        { id: 3, name: "Կանացի բոթասներ", slug: "կանացի-բոթասներ" },
      ],
      variants: [],
      description: {
        location: [
          { id: 1, name: "Արտադրված է Հայաստանում" }
        ],
        material: [
          { id: 1, name: "Դեռմանտին" },
        ],
        color: [
          { id: 1, name: "Դեղին" },
          { id: 2, name: "Կանաչ" },
          { id: 5, name: "Մոխրագույն" },
          { id: 7, name: "Սև" },
          { id: 8, name: "Սպիտակ" }
        ],
        size: [
          { id: 1, name: "34" },
          { id: 3, name: "35+" },
          { id: 4, name: "36" },
          { id: 6, name: "38" },
          { id: 7, name: "39" },
          { id: 8, name: "40" },
          { id: 10, name: "42" },
          { id: 11, name: "43" }
        ]
      },
      pictures: [],
      review: 1,
      sm_pictures: [],
      slug: "new-iamghe",
      new: ''
    },
    {
      id: 87,
      name: "կանացի սպորտային կոշիկ՝ կաշվիցaaaaaaaaaaaaaaaaaaaaaaa",
      slug: "կանացի-սպորտային-կոշիկ՝-կաշվից",
      short_desc: "Sed egestas, ante et vulputate volutpat, eros pede semper est, vitae luctus metus libero eu augue. Morbi purus libero, faucibus adipiscing. Sed lectus.",
      location: { id: 1, name: "Արտադրված է Հայաստանում" },
      material: [
        { id: 1, name: 'Դեռմանտին ' },
        { id: 2, name: 'Կաշվե' }
      ],
      price: 32000,
      sale_price: 12000,
      code: 4263,
      review: 2,
      ratings: 4,
      until: null,
      stock: 100,
      top: true,
      featured: null,
      new: null,
      author: null,
      sold: null,
      status: {
        title: "Առկա է ",
        value: "instock"
      },
      category: [
        { id: 2, name: "Բոթասներ", slug: "բոթասներ" },
        { id: 3, name: "Կանացի բոթասներ", slug: "կանացի-բոթասներ" }
      ],
      brands: [
        {
          name: "UGG",
          slug: "ugg"
        }
      ],
      pictures: [
        {
          width: "800",
          height: "800",
          url: "/product_images/prod5.jpg"
        },
        {
          width: "800",
          height: "800",
          url: "/product_images/prod5.jpg"
        },
      ],
      sm_pictures: [
        {
          width: "300",
          height: "300",
          url: "/product_images/prod5.jpg"
        },
        {
          width: "300",
          height: "300",
          url: "/product_images/prod5.jpg"
        },
      ],
      variants: [
        {
          id: 1,
          color: "#333333",
          color_name: "Black",
          price: 75,
          size: [
            {
              id: 1,
              name: "Small",
              slug: null
            },
            {
              id: 3,
              name: "Medium",
              slug: null
            },
            {
              id: 2,
              name: "Large",
              slug: null
            },
            {
              id: 4,
              name: "Extra Large",
              slug: null
            }
          ],
        }
      ],
    },
  ],
  newOrEditProduct: {
    productName: '',
    productRegularPrice: '',
    productSalePrice: '',
    status: {
      title: "պահեստում",
      value: ''
    },
    weight: '',
    dimensions: {
      length: '',
      width: '',
      height: ''
    },
    productsCategories: [
      { id: 1, name: "Պայուսակներ", slug: "պայուսակներ", checked: false },
      { id: 2, name: "Բոթասներ", slug: "բոթասներ", checked: false },
      { id: 3, name: "Կանացի բոթասներ", slug: "կանացի-բոթասներ", checked: false },
      { id: 4, name: "Տղամարդու բոթասներ", slug: "տղամարդու-բոթասներ", checked: false },
      { id: 5, name: "Կոշիկներ", slug: "կոշիկներ", checked: false },
      { id: 6, name: "Կանանցի կոշիկներ", slug: "կանանցի-կոշիկներ", checked: false },
      { id: 7, name: "Տղամարդու կոշիկներ", slug: "տղամարդու-կոշիկներ", checked: false },
      { id: 8, name: "Գոտիներ", slug: "գոտիներ", checked: false },
      { id: 9, name: "Կանանցի գոտիներ", slug: "կանանցի-գոտիներ", checked: false },
      { id: 10, name: "Տղամարդու գոտիներ", slug: "տղամարդու-գոտիներ", checked: false },
    ],
    category: [],
    pictures: [],
    prodPhotoInputValue: '',
    prodAttributes: {
      selectAttributeValue: '',
      location: {
        addNew: '',
        name: "location",
        title: "Արտադրության վայր",
        addAttr: false,
        addCategory: false,
        categories_radio: [
          { id: 1, name: "Արտադրված է Հայաստանում", checked: false },
          { id: 2, name: "Արտադրված է rd", checked: false },
          { id: 3, name: "Արտադրված է usa", checked: false },
          { id: 4, name: "Արտադրված է ch", checked: false },
          { id: 5, name: "Արտադրված է uk", checked: false },
        ]
      },
      material: {
        addNew: '',
        name: "material",
        title: "Մատերիալ",
        addAttr: false,
        addCategory: false,
        categories: [
          { id: 1, name: "Դեռմանտին", checked: false },
          { id: 2, name: "Կաշվե", checked: false }
        ]
      },
      variant: {
        selectAttributeVariantValue: '',
        addNew: '',
        name: "variant",
        title: "Տարբերակներ",
        addAttr: false,
        // addCategory: false,
        colors: [
          { id: 1, color: "#FFFF00", color_name: "yellow", title: "Դեղին", selected: false },
          { id: 2, color: "#00FF00", color_name: "green", title: "Կանաչ", selected: false },
          { id: 3, color: "#3399cc", color_name: "blue", title: "Կապույտ", selected: false },
          { id: 4, color: "#cc3333", color_name: "red", title: "Կարմիր", selected: false },
          { id: 5, color: "#808080", color_name: "gray", title: "Մոխրագույն", selected: false },
          { id: 6, color: "#FFA500", color_name: "orange", title: "Նարնջագույն", selected: false },
          { id: 7, color: "#000000", color_name: "black", title: "Սև", selected: false },
          { id: 8, color: "#ffffff", color_name: "white", title: "Սպիտակ", selected: false }
        ],
        sizes: [
          { id: 1, name: "34", slug: null },
          { id: 2, name: "35", slug: null },
          { id: 3, name: "35+", slug: null },
          { id: 4, name: "36", slug: null },
          { id: 5, name: "37", slug: null },
          { id: 6, name: "38", slug: null },
          { id: 7, name: "39", slug: null },
          { id: 8, name: "40", slug: null },
          { id: 9, name: "41", slug: null },
          { id: 10, name: "42", slug: null },
          { id: 11, name: "43", slug: null },
          { id: 12, name: "S", slug: null },
          { id: 13, name: "XS", slug: null },
          { id: 14, name: "M", slug: null },
          { id: 15, name: "L", slug: null },
          { id: 16, name: "XL", slug: null },
          { id: 17, name: "XXL", slug: null },
        ],

      },
      variants: [],
    }
  },
}
let a = 2;
const changeState = (state = initialState, { type, ...rest }) => {
  let newCat;
  switch (type) {
    case 'set':
      return { ...state, ...rest }
    case 'DELETE_USER':
      return {
        ...state,
        products: [
          ...state.products.filter(el => el.id !== rest.id)
        ]
      }
    case 'DELETE_PRODUCT': {
      return {
        ...state,
        products: [...state.products.filter(item => item.id !== rest.id)]
      }
    }
    case 'NEW_PRODUCT_NAME':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productName: rest.value
        }
      }
    case 'NEW_PRODUCT_REGULAR_PRICE':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productRegularPrice: rest.value
        }
      }
    case 'NEW_PRODUCT_SALE_PRICE':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productSalePrice: rest.value
        }
      }
    case 'NEW_PRODUCT_IN_STOCK':
      if (rest.value === "instock") {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            status: {
              title: "Առկա է ",
              value: rest.value
            },
          }
        }
      }
      else if (rest.value === "outofstock") {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            status: {
              title: "Առկա չէ ",
              value: rest.value
            },
          }
        }
      }
      return state
    case 'NEW_PRODUCT_WEIGHT':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          weight: rest.value
        }
      }
    case 'NEW_PRODUCT_DIMENSIONS_LENGTH':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            length: rest.value
          }
        }
      }
    case 'NEW_PRODUCT_DIMENSIONS_WIDTH':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            width: rest.value
          }
        }
      }
    case 'NEW_PRODUCT_DIMENSIONS_HEIGHT':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            height: rest.value
          }
        }
      }
    case 'NEW_PRODUCT_CATEGORY':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productsCategories: [
            ...state.newOrEditProduct.productsCategories.map(
              (content, i) => i + 1 === rest.value ? { ...content, checked: !content.checked }
                : content
            )
          ],
          category:
            state.newOrEditProduct.category.some(category => (category.id === rest.value)) ?
              state.newOrEditProduct.category.filter(el => (el.id !== rest.value))
              : [
                ...state.newOrEditProduct.category,
                state.newOrEditProduct.productsCategories.map(el => el.id === rest.value ? { id: el.id, name: el.name, slug: el.slug } : el).filter(el => el.id === rest.value)[0]
              ]
        }
      }
    case 'NEW_PRODUCT_LOCATION_ATTRIBUTE_CATEGORY':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              categories_radio: state.newOrEditProduct.prodAttributes.location.categories_radio.map((content, i) =>
                content.id === rest.id ? { ...content, checked: !content.checked }
                  : { ...content, checked: false })
            }
          }
        }
      }
    case 'NEW_PRODUCT_MATERIAL_ATTRIBUTE_CATEGORY':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              categories: [...state.newOrEditProduct.prodAttributes.material.categories.map((content, i) =>
                content.id === rest.id ? { ...content, checked: !content.checked }
                  : content)
              ]
            }
          }
        }
      }
    case 'NEW_PRODUCT_UPLOAD_PHOTO':
      // console.log(rest.value.map(img => URL.createObjectURL(img)));
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          pictures: [
            ...state.newOrEditProduct.pictures,
            { width: 800, height: 800, url: URL.createObjectURL(rest.url) }
          ]
        }
      }
    case 'REMOVE_PRODUCT_IMAGE':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          pictures: state.newOrEditProduct.pictures.filter(image => image.url !== rest.url)
        }
      }
    case 'CHANGE_TYPE_LOCATION':
      if (rest.value) {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              location: { ...state.newOrEditProduct.prodAttributes.location, addAttr: rest.value }
            }
          }
        }
      } else {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              location: { ...state.newOrEditProduct.prodAttributes.location, addAttr: rest.value, addCategory: rest.value }
            }
          }
        }
      }
    case 'CHANGE_TYPE_MATERIAL':
      if (rest.value) {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              material: { ...state.newOrEditProduct.prodAttributes.material, addAttr: rest.value }
            }
          }
        }
      } else {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              material: { ...state.newOrEditProduct.prodAttributes.material, addAttr: rest.value, addCategory: rest.value }
            }
          }
        }
      }
    case 'CHANGE_TYPE_VARIANT':
      if (rest.value) {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              variant: { ...state.newOrEditProduct.prodAttributes.variant, addAttr: rest.value }
            }
          }
        }
      } else {
        return {
          ...state,
          newOrEditProduct: {
            ...state.newOrEditProduct,
            prodAttributes: {
              ...state.newOrEditProduct.prodAttributes,
              variant: { ...state.newOrEditProduct.prodAttributes.variant, addAttr: rest.value, addCategory: rest.value }
            }
          }
        }
      }
    case 'ADD_CATEGORY_LOCATION':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: { ...state.newOrEditProduct.prodAttributes.location, addCategory: rest.value }
          }
        }
      }
    case 'ADD_CATEGORY_MATERIAL':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            material: { ...state.newOrEditProduct.prodAttributes.material, addCategory: rest.value }
          }
        }
      }
    // case 'ADD_CATEGORY_VARIANT':
    //   return {
    //     ...state,
    //     newOrEditProduct: {
    //       ...state.newOrEditProduct,
    //       prodAttributes: {
    //         ...state.newOrEditProduct.prodAttributes,
    //         variant: { ...state.newOrEditProduct.prodAttributes.variant, addCategory: rest.value }
    //       }
    //     }
    //   }
    case 'CHANGE_SELECT_ATTR_VAL':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: { ...state.newOrEditProduct.prodAttributes, selectAttributeValue: rest.value }
        }
      }
    case 'CHANGE_SELECT_ATTR_VARIANT_VAL':
      const colorVariant = state.newOrEditProduct.prodAttributes.variant.colors.filter(color => color.id === rest.id)[0]
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            variant: {
              ...state.newOrEditProduct.prodAttributes.variant,
              colors: [
                ...state.newOrEditProduct.prodAttributes.variant.colors.map(color => {
                  return (color.id === rest.id ? { ...color, selected: !color.selected } : color)
                })
              ],
            },
            variants: colorVariant.selected
              ? state.newOrEditProduct.prodAttributes.variants.filter(el => el.id !== rest.id)
              : [...state.newOrEditProduct.prodAttributes.variants, {
                id: rest.id,
                color: colorVariant.color,
                color_name: colorVariant.title,
                price: '',
                size: [],
              }]
          }
        }
      }
    case 'ADD_SIZE_TO_VARIANT':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            variants: state.newOrEditProduct.prodAttributes.variants.map((content) => {
              return (content.id === rest.id
                ? {
                  ...content,
                  size: content.size.length > 0
                    ? content.size.some(el => {
                      return (el.id === rest.val)
                    }) ? content.size
                      : [
                        ...content.size,
                        state.newOrEditProduct.prodAttributes.variant.sizes.filter(el => {
                          return (el.id === rest.val)
                        })[0]
                      ]
                    : [
                      ...content.size,
                      state.newOrEditProduct.prodAttributes.variant.sizes.filter(el => {
                        return (el.id === rest.val)
                      })[0]
                    ]
                }
                : content
              )
            })
          }
        }
      }
    case 'REMOVE_SIZE_IN_VARIANT':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            variants: state.newOrEditProduct.prodAttributes.variants.map((content) => {
              return (content.id === rest.variantId
                ? {
                  ...content,
                  size:
                    content.size.filter(el => {
                      return (el.id !== rest.sizeId)
                    })
                }
                : content
              )
            })
          }
        }
      }
    case 'CHANGE_ATTR_CATEGORY_VAL':
      return {
        ...state,
        selectAttributeValue: rest.value
      }
    case 'CHANGE_LOCATION_CATEGORY_VAL':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: { ...state.newOrEditProduct.prodAttributes.location, addNew: rest.value }
          }
        }
      }
    case 'CHANGE_MATERIAL_CATEGORY_VAL':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            material: { ...state.newOrEditProduct.prodAttributes.material, addNew: rest.value }
          }
        }
      }
    case 'CHANGE_VARIANT_CATEGORY_VAL':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            variant: { ...state.newOrEditProduct.prodAttributes.variant, addNew: rest.value }
          }
        }
      }
    case 'ADD_LOCATION_CATEGORY':
      newCat = { id: a++, name: state.newOrEditProduct.prodAttributes.location.addNew, checked: false }
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              categories_radio: [...state.newOrEditProduct.prodAttributes.location.categories_radio, newCat],
              addNew: "",
              addCategory: false,
            }
          }
        }
      }
    case 'ADD_MATERIAL_CATEGORY':
      newCat = { id: 22, name: state.newOrEditProduct.prodAttributes.material.addNew, checked: false }
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              categories: [...state.newOrEditProduct.prodAttributes.material.categories, newCat],
              addNew: "",
              addCategory: false,
            }
          }
        }
      }
    case 'ADD_VARIANT_CATEGORY':
      newCat = { id: 22, name: state.newOrEditProduct.prodAttributes.variant.addNew, checked: false }
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            variant: {
              ...state.newOrEditProduct.prodAttributes.variant,
              categories: [...state.newOrEditProduct.prodAttributes.variant.categories, newCat],
              addNew: "",
              addCategory: false,
            }
          }
        }
      }
    case 'ADD_NEW_PRODUCT':
      return {
        ...state,
        products: [
          ...state.products, {
            id: 22,
            name: state.newOrEditProduct.productName,
            price: state.newOrEditProduct.productRegularPrice,
            sale_price: state.newOrEditProduct.productSalePrice,
            ratings: '',
            status: state.newOrEditProduct.status,
            weight: state.newOrEditProduct.weight,
            dimensions: state.newOrEditProduct.dimensions,
            category: state.newOrEditProduct.category,
            location: state.newOrEditProduct.prodAttributes.location.categories_radio.filter(el => el.checked).map(el => ({ id: el.id, name: el.name }))[0],
            material: state.newOrEditProduct.prodAttributes.material.categories.filter(el => el.checked).map(el => ({ id: el.id, name: el.name })),
            pictures: state.newOrEditProduct.pictures,
            review: 2,
            sm_pictures: [],
            variants: state.newOrEditProduct.prodAttributes.variants,
            slug: state.newOrEditProduct.productName.toLowerCase().split(' ').join('-'),
            __typename: "Product",
            new: null
          }
        ],
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productName: '',
          productRegularPrice: '',
          productSalePrice: '',
          status: {
            ...state.newOrEditProduct.status,
            title: "պահեստում",
            value: ''
          },
          weight: '',
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            length: '',
            width: '',
            height: ''
          },
          productsCategories: [
            ...state.newOrEditProduct.productsCategories.map((content) => {
              return ({
                ...content,
                checked: false
              })
            })
          ],
          category: [],
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              addAttr: false,
              addCategory: false,
              categories_radio: [
                ...state.newOrEditProduct.prodAttributes.location.categories_radio.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              addAttr: false,
              addCategory: false,
              categories: [
                ...state.newOrEditProduct.prodAttributes.material.categories.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            variant: {
              ...state.newOrEditProduct.prodAttributes.variant,
              addAttr: false,
              addCategory: false,
              colors: [
                ...state.newOrEditProduct.prodAttributes.variant.colors.map((content) => {
                  return ({
                    ...content,
                    selected: false
                  })
                }),
              ]
            },
            variants: [],
          },
          pictures: [],
          prodPhotoInputValue: '',
        }
      }
    //vorpeszi cancel anelu depqum himnakan tvyalnery chpoxven 
    case 'CHANGE_PRODUCT':
      let currentProduct = state.products.filter(product => product.id === rest.id)[0]
      return {
        ...state,
        newOrEditProduct: {



          // name: state.newOrEditProduct.productName,
          // price: state.newOrEditProduct.productRegularPrice,
          // sale_price: state.newOrEditProduct.productSalePrice,
          // ratings: '',
          // status: state.newOrEditProduct.status,
          // weight: state.newOrEditProduct.weight,
          // dimensions: state.newOrEditProduct.dimensions,
          // category: state.newOrEditProduct.category,



          // location: state.newOrEditProduct.prodAttributes.location.categories_radio.filter(el => el.checked).map(el => ({ id: el.id, name: el.name })),
          // material: state.newOrEditProduct.prodAttributes.material.categories.filter(el => el.checked).map(el => ({ id: el.id, name: el.name })),
          // pictures: state.newOrEditProduct.pictures,
          // review: 2,
          // sm_pictures: [],
          // variants: state.newOrEditProduct.prodAttributes.variants,
          // slug: state.newOrEditProduct.productName.toLowerCase().split(' ').join('-'),
          // __typename: "Product",
          // new: null,



          ...state.newOrEditProduct,
          productName: currentProduct.name,
          category: currentProduct.category,
          pictures: currentProduct.pictures,
          prise: currentProduct.price,
          sale_price: currentProduct.sale_price,
          ratings: currentProduct.ratings,
          status: currentProduct.status,
          weight: currentProduct.weight,
          dimensions: currentProduct.dimensions,
          productsCategories: state.newOrEditProduct.productsCategories.map(el =>
            currentProduct.category.some(cat => cat.id === el.id) ? { ...el, checked: !el.checked } : el),
          // location: state.newOrEditProduct.prodAttributes.location.categories_radio.filter(el => el.checked).map(el => ({ id: el.id, name: el.name })),
          // material: state.newOrEditProduct.prodAttributes.material.categories.filter(el => el.checked).map(el => ({ id: el.id, name: el.name })),
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              addAttr: currentProduct.location ? true : false,
              categories_radio: state.newOrEditProduct.prodAttributes.location.categories_radio.map(el =>
                (currentProduct.location.id === el.id) ? { ...el, checked: !el.checked } : el),
            },
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              addAttr: currentProduct.material ? true : false,
              categories: state.newOrEditProduct.prodAttributes.material.categories.map(el => {
                return (currentProduct.material.some(cat => cat.id === el.id)) ? { ...el, checked: !el.checked } : el
              }),
            }
          }
        },
      }
    case 'SAVE_CHANGES':
      return {
        ...state,
        products: state.products.map(el => {
          return (el.id === rest.id ? {
            ...el,
            name: state.newOrEditProduct.productName,
            category: state.newOrEditProduct.category,
            pictures: state.newOrEditProduct.pictures,
          } : el)
        }),
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productName: '',
          productRegularPrice: '',
          productSalePrice: '',
          status: {
            ...state.newOrEditProduct.status,
            title: "պահեստում",
            value: ''
          },
          weight: '',
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            length: '',
            width: '',
            height: ''
          },
          productsCategories: [
            ...state.newOrEditProduct.productsCategories.map((content) => {
              return ({
                ...content,
                checked: false
              })
            })
          ],
          category: [],
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              addAttr: false,
              addCategory: false,
              categories_radio: [
                ...state.newOrEditProduct.prodAttributes.location.categories_radio.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              addAttr: false,
              addCategory: false,
              categories: [
                ...state.newOrEditProduct.prodAttributes.material.categories.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            variant: {
              ...state.newOrEditProduct.prodAttributes.variant,
              addAttr: false,
              addCategory: false,
              colors: [
                ...state.newOrEditProduct.prodAttributes.variant.colors.map((content) => {
                  return ({
                    ...content,
                    selected: false
                  })
                }),
              ]
            },
            variants: [],
          },
          pictures: [],
          prodPhotoInputValue: '',
        }
      }
    case 'REMOVE_CHANGES':
      return {
        ...state,
        newOrEditProduct: {
          ...state.newOrEditProduct,
          productName: '',
          productRegularPrice: '',
          productSalePrice: '',
          status: {
            ...state.newOrEditProduct.status,
            title: "պահեստում",
            value: ''
          },
          weight: '',
          dimensions: {
            ...state.newOrEditProduct.dimensions,
            length: '',
            width: '',
            height: ''
          },
          productsCategories: [
            ...state.newOrEditProduct.productsCategories.map((content) => {
              return ({
                ...content,
                checked: false
              })
            })
          ],
          category: [],
          prodAttributes: {
            ...state.newOrEditProduct.prodAttributes,
            location: {
              ...state.newOrEditProduct.prodAttributes.location,
              addAttr: false,
              addCategory: false,
              categories_radio: [
                ...state.newOrEditProduct.prodAttributes.location.categories_radio.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            material: {
              ...state.newOrEditProduct.prodAttributes.material,
              addAttr: false,
              addCategory: false,
              categories: [
                ...state.newOrEditProduct.prodAttributes.material.categories.map((content) => {
                  return ({
                    ...content,
                    checked: false
                  })
                }),
              ]
            },
            variant: {
              ...state.newOrEditProduct.prodAttributes.variant,
              addAttr: false,
              addCategory: false,
              colors: [
                ...state.newOrEditProduct.prodAttributes.variant.colors.map((content) => {
                  return ({
                    ...content,
                    selected: false
                  })
                }),
              ]
            },
            variants: [],
          },
          pictures: [],
          prodPhotoInputValue: '',
        }
      }
    default:
      return state
  }
}

const store = createStore(changeState)
window.store = store
export default store