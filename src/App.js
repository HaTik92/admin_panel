import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './scss/style.scss';
import Cookies from 'js-cookie'

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
)

// Containers
const TheLayout = React.lazy(() => import('./containers/TheLayout'));

// Pages
const Login = React.lazy(() => import('./views/pages/login/Login'));
// const Register = React.lazy(() => import('./views/pages/register/Register'));
const Page404 = React.lazy(() => import('./views/pages/page404/Page404'));
const Page500 = React.lazy(() => import('./views/pages/page500/Page500'));

console.log("AAA");


const App = () => {
  const dispatch = useDispatch()
  const isAuth = useSelector(state => state.login.isAuth)
  const adminIsAuth = () => {
    const admin = Cookies.get('admin')
    if (admin) {
      return dispatch({ type: 'IS_AUTH', value: true })
    }
  }
  React.useEffect(() => {
    adminIsAuth();
  })
  if (!isAuth) {
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            <Route exact path="/login" name="Login Page" render={props => <Login {...props} />} />
            <Redirect to="/login" />
          </Switch>
        </React.Suspense>
      </BrowserRouter>)
  }
  Cookies.set('admin', 'isAuth')
  return (
    <BrowserRouter>
      <React.Suspense fallback={loading}>
        <Switch>
          {/* <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} /> */}
          <Route exact path="/404" name="Page 404" render={props => <Page404 {...props} />} />
          <Route exact path="/500" name="Page 500" render={props => <Page500 {...props} />} />
          <Route path="/" name="Home" render={props => <TheLayout {...props} />} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  )
}


export default App;
